<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Rest::class ) ) {
	class Rest {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'register_routes'] );
		}

		public static function get_forms( \WP_REST_Request $request ) {
			$all_forms = Ninja_Forms()->form()->get_forms();

			$forms = [];
			foreach ( $all_forms as $form ) {
				$forms[] = [
					'value' => $form->get_id(),
					'label' => $form->get_settings()['title'],
				];
			}
			//return $forms;
			return json_encode( $forms );
		}

		public function register_routes() {
			\register_rest_route( 'jensen/v1', '/ninja/forms/', [
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => [$this, 'get_forms'],
				'permission_callback' => function () {
					return true;
					return current_user_can( 'edit_post' );
				},
			] );
		}
	}

	new Rest();
}