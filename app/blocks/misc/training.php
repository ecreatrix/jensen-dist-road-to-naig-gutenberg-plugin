<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockTraining::class ) ) {
	class BlockTraining {
		public function __construct() {
			add_action( 'init', [$this, 'register_block_main'], 20 );
			add_action( 'init', [$this, 'register_block_subblock'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block_main() {
			$block_json_file = BlockHelpers::block_json( 'training' );
			register_block_type_from_metadata( $block_json_file );
		}

		function register_block_subblock() {
			$block_json_file = BlockHelpers::block_json( 'training-subblock' );
			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			// Remove the block/timed-block from the rendered content.
			if ( 'jg/training' === $block['blockName'] ) {
				$attributes = $block['attrs'];
				$title      = $attributes['title'];
				$slug       = sanitize_title_with_dashes( str_replace( ['"', "'", '“', '”'], "", $title ) );
				$blockId    = $attributes['blockId'];

				$post_id     = get_the_ID() . ' ' . $title;
				$helpersUser = new \jg\Theme\HelpersUser();
				$user_id     = $helpersUser->get_user_id();
				$form_id     = $attributes['nfUpload'];

				$buttons = BlockHelpers::user_completion( $post_id, $user_id, $attributes );

				$user_id = get_current_user_id();

				$title = '<div class="accordion-title collapse-icon collapsed" data-bs-toggle="collapse" href="#' . $blockId . '" role="button" aria-expanded="false" aria-controls="' . $blockId . '"><span class="fa-stack collapse-icon"><i class="fas fa-circle fa-stack-2x"></i><i class="fas fa-caret-down fa-stack-1x fa-inverse"></i></span>' . $title . '</div>';

				$classes = ['text'];
				if ( $slug !== $_GET['block'] ) {
					$classes[] = 'collapse';
				}

				$content = '<div class="' . implode( ' ', $classes ) . '" id="' . $blockId . '">' . BlockHelpers::render_restriction( $block_content . $buttons, $attributes, $type ) . '</div>';

				$block_content = '<div id="' . $slug . '" class="' . implode( ' ', ['wp-block-jg-training', $attributes['className']] ) . '">' . $title . $content . '</div>';
			}

			return $block_content;
		}
	}

	new BlockTraining();
}