<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockVisibility::class ) ) {
	class BlockVisibility {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'content-visibility' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			// Remove the block/timed-block from the rendered content.
			if ( 'jg/content-visibility' === $block['blockName'] ) {
				$attributes = $block['attrs'];
				$classes    = ['visibility', 'wp-block-cover'];

				$block_content = BlockHelpers::render_restriction( $block_content, $attributes );
			}

			return $block_content;
		}
	}

	new BlockVisibility();
}