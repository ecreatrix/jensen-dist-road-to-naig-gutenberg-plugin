<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockHelpers::class ) ) {
	class BlockHelpers {
		public function __construct() {}
		static public function block_json( $name, $file = 'block.json' ) {
			return \jg\Plugin\Gutenberg\PATH . 'assets/scripts/blocks/' . $name . '/';
		}

		// Edit/delete buttons
		static function buttons_change( $type, $id ) {
			$query_user_id = isset( $_GET['user_id'] ) ? '&user_id=' . $_GET['user_id'] : '';

			$labels = [
				'edit'   => '<i class="fas fa fa-edit"> Edit</i>',
				'delete' => '<i class="fas fa fa-times"> Remove</i>',
			];
			$buttons = [];

			foreach ( $labels as $key => $label ) {
				if ( 'family' === $type && 'delete' === $key && 'profile' === $id ) {
					continue;
				} else if ( 'activity' === $type && 'edit' === $key ) {
					continue;
				}

				$buttons[] = '<a class="btn ' . $key . '" href="?type=' . $type . '&action=' . $key . '&id=' . $id . $query_user_id . '">' . $label . '</a>';
			}

			return '<span class="links">' . implode( $buttons ) . '</span>';
		}

		// Show delete buttons
		static function buttons_delete( $type, $user_id ) {
			$delete_type = $_GET['type'] ? $_GET['type'] : false;
			if ( $type !== $delete_type ) {
				return;
			}

			$should_delete = self::should_delete( $user_id );

			$delete_id = isset( $_GET['id'] ) ? $_GET['id'] : false;
			$content   = '';

			$family = \jg\Theme\HelpersUser::get_family( $user_id );

			if ( 'family' === $type && $family ) {
				$member = $family[$should_delete];
				$label  = $member['first_name'] . ' ' . $member['last_name'];
				$points = $member['points'] ? $member['points'] : 0;

				$content = 'If you proceed, <u>' . $points . '</u> points will be deducted from your account.';
			}

			if ( $should_delete ) {
				global $post;
				$post_id      = $post->ID;
				$profile_page = \jg\Theme\HelpersTheme::theme_page( $post_id, 'profile' );

				$permalink = $profile_page['permalink'] . '?type=' . $type;

				if ( ! $profile_page['current_page'] ) {
					$permalink = get_permalink( $post_id ) . '?type=' . $type;
				}

				$permalink = $permalink . $profile_page['append_query_user'];

				$irreversible = '<h5 class="mb-3 text-secondary"><u>This change is permanent</u></h5>';
				$remove       = '<h5 class="mb-3">Would you like to remove ' . $label . ' from your account?</h5>';

				$content = $remove . $irreversible . '<p>' . $content . '</p><a class="btn btn-tertiary" href="' . $permalink . '&action=delete_confirm&id=' . $should_delete . '">Remove ' . $label . '</a>
				<a class="ml-4 btn btn-secondary" href="' . $permalink . '">Keep ' . $label . '</a>';

				return $content;
			}

			return false;
		}

		static function color_object() {
			return [
				'type'       => 'object',
				'properties' => [
					'color' => [
						'type'    => 'string',
						'default' => '#ffffff',
						'format'  => 'hex-color',
					],
					'name'  => [
						'type'    => 'string',
						'default' => '',
					],
				],
			];
		}

		static function cover_block( $classes, $content, $attributes = '' ) {
			$content = '<div class="wp-block-cover__inner-container">' . $content . '</div>';

			if ( ! $classes || ! is_array( $classes ) ) {
				return $content;
			}

			$classes[] = 'wp-block-cover jg';

			$style = '';
			if ( ! empty( $attributes ) ) {
				$style = ' style="' . implode( '; ', $attributes ) . '"';
			}

			return '<div class="' . implode( ' ', $classes ) . '" class="' . implode( ' ', $classes ) . '"' . $style . '>' . $content . '</div>';
		}

		static function empty_table( $type, $colspan = 1 ) {
			return '<td colspan="' . $colspan . '">No ' . $type . ' entries yet</td>';
		}

		static function games_ended( $text = false, $end_date = false ) {
			if ( ! $end_date ) {
				$end_date = get_theme_mod( 'jg_game_end' );
			}

			if ( ! $text ) {
				$text = 'This activity is now closed. Thank you for the participation!';
			}

			$end_date = strtotime( $end_date );

			if ( $end_date < strtotime( 'now' ) ) {
				// Games have ended
				return '<h4>' . $text . '</h4>';
			}

			return false;
		}

		static function games_started( $text = false, $start_date = false, $date_in_text = true ) {
			if ( ! $start_date ) {
				$start_date = get_theme_mod( 'jg_game_start' );
			}

			$start_date = strtotime( strstr( $start_date, 'T', true ) );

			if ( ! $text && $date_in_text ) {
				$text = 'These activities will be available on <u>' . date( 'F j, Y', $start_date ) . '</u>, when the program starts, for participants to complete. Make sure you register and check back here to get started.';
			} else if ( ! $text && ! $date_in_text ) {
				$text = 'These activities are available for participants to complete. Make sure you register and check back here to get started.';
			}

			if ( $start_date > strtotime( 'now' ) ) {
				// Games have not started
				return '<h4 class="text-center">' . $text . '</h4>';
			}

			return false;
		}

		static function generic_error_message() {
			return self::cover_block( [], '<h1 class="mx-auto text-center mb-4">Error</h1><h3 class="mx-auto text-center">Please contact us for support.</h3>' );
		}

		static function get_action_label( $tab_slug, $tab_label = false ) {
			$label = 'Add';

			if ( ! $tab_label ) {
				$tab_label = $tab_slug;
			}

			if ( $_GET['action'] && $_GET['type'] && $tab_slug === $_GET['type'] && 'delete_confirm' !== $_GET['action'] ) {
				$label = 'delete' === $_GET['action'] ? 'Remove' : 'Modify';
			}

			return '<hr><h4 class="mt-5 mb-3">' . $label . ' ' . $tab_label . '</h4>';
		}

		static function get_sport_info( $sportID ) {
			$sport      = get_post( $sportID );
			$sport_meta = get_post_meta( $sportID );

			$terms = get_the_terms( $sportID, 'sport_tax' );

			return $terms[0]->name . ' - ' . $sport->post_title;
		}

		static function get_sports_terms( $attributes ) {
			$terms = get_terms( [
				'taxonomy'   => 'sport_tax',
				'per_page'   => 1,
				'hide_empty' => false,
			] );

			if ( array_key_exists( 'category', $attributes ) && $attributes['category'] && $attributes['category'] && 'false' !== $attributes['category'] ) {
				$terms = get_term_by( 'slug', $attributes['category'], 'sport_tax' );
			}

			return $terms;
		}

		static function get_workshop_info() {
			return '';
		}

		static function gutenberg_classes( $classes, $attributes ) {
			if ( array_key_exists( 'align', $attributes ) ) {
				$classes[] = "align{$attributes['align']}";
			}

			if ( array_key_exists( 'backgroundColor', $attributes ) ) {
				$classes[] = "has-{$attributes['backgroundColor']}-background-color";
			}

			if ( array_key_exists( 'textColor', $attributes ) ) {
				$classes[] = "has-{$attributes['textColor']}-text-color";
			}

			return $classes;
		}

		static public function make_modal( $slug, $button_content, $modal_content, $button_class = '' ) {
			$classes = ['btn btn-link btn-modal', $button_class];
			$button  = '<a class="' . implode( ' ', $classes ) . '" data-toggle="modal" data-target="#' . $slug . '">' . $button_content . '</a>';

			$modal = '<div class="modal fade" id="' . $slug . '" tabindex="-1" role="dialog" aria-labelledby="' . $slug . '" aria-hidden="true"><div class="modal-dialog modal-dialog-centered modal-xl" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="' . $slug . '-title">Uploaded Media</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">' . $modal_content . '</div>  <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div></div></div></div>';

			return [
				'button'  => $button,
				'content' => $modal,
			];
		}

		static function page_link( $content ) {
			global $post;
			$post_id = $post->ID;

			$user = get_user_by( 'ID', \jg\Theme\HelpersUser::get_user_id() );

			if ( ! method_exists( '\jg\Theme\Helpers', 'theme_page' ) ) {
				return false;
			}

			$profile_page          = \jg\Theme\HelpersTheme::theme_page( $post_id, 'profile' );
			$signin_page           = \jg\Theme\HelpersTheme::theme_page( $post_id, 'signin' );
			$registration_page     = \jg\Theme\HelpersTheme::theme_page( $post_id, 'registration_start' );
			$registration_complete = \jg\Theme\HelpersTheme::theme_page( $post_id, 'registration_complete' );

			if ( $signin_page['current_page'] && is_user_logged_in() ) {
				// On signing page page and logged in, show profile link

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $profile_page['permalink'] . '" style="color: inherit; text-decoration: underline">View your profile here</a></h3>' );
			} else if ( $profile_page['current_page'] && ! is_user_logged_in() ) {
				// On profile page and not logged in, show registration link

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $signin_page['permalink'] . '" style="color: inherit; text-decoration: underline">Sign in here</a></h3>' );
			} else if ( $registration_page['current_page'] && is_user_logged_in() ) {
				// On registration page and logged in, show profile link

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $profile_page['permalink'] . '" style="color: inherit; text-decoration: underline">View your profile here</a></h3>' );
			} else if ( $registration_complete['current_page'] && is_user_logged_in() && ! in_array( 'subscriber', (array) $user->roles ) ) {
				// On finish registration

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $profile_page['permalink'] . '" style="color: inherit; text-decoration: underline">View your profile here</a></h3>' );
			}

			return false;
		}

		static public function post_datetime( $id ) {
			$date_meta = strtotime( get_post_meta( $id, 'jg_timedate', true ) );

			$key = intval( $date_meta ? $date_meta : strtotime( '+3 years' ) . wp_rand() );

			$content = '';
			if ( '' !== $date_meta ) {
				$date = '<span class="date">Date: <span class="has-primary-color">' . date( 'jS F', $date_meta ) . '</span></span>';

				$time = '';
				if ( date( 'H:i A', $date_meta ) !== '00:00 AM' ) {
					$time = '<span class="time">Time: <span class="has-primary-color">' . date( 'H:i A', $date_meta ) . '</span></span>';
				}

				$content = $date . $time;
			}

			return ['content' => $content, 'key' => $key];
		}

		static public function remove_event( $user_id, $delete_id, $event_key ) {
			$edition = \jg\Theme\HelpersUser::get_edition();

			$meta  = get_user_meta( $user_id, $edition, true );
			$event = $meta[$event_key][$delete_id];
			if ( ! $event || empty( $event ) ) {
				return false;
			}

			$family = \jg\Theme\HelpersUser::get_family( $user_id );
			if ( $family ) {
				$member          = $family[$delete_id];
				$event_submember = $member[$event_key];
				foreach ( $event_submember as $id => $points ) {
					if ( 'uploads' === $event_key ) {
						// Upload points are not stored in user meta
						$event_points = (int) get_theme_mod( 'jg_points_fitness_initial' );
						if ( 'fitness-final' === $upload_type ) {
							$event_points = (int) get_theme_mod( 'jg_points_fitness_final' );
						}
						$search_key = array_search( $points, $event[$id] );

						if ( false !== $search_key ) {
							unset( $event[$id][$search_key] );
						}
					} else {
						$current_points = $event[$id];
						$new_points     = $current_points - $points;
						$event[$id]     = $new_points;

						if ( 0 == $new_points ) {
							unset( $event[$id] );
						}
					}
				}
			} else {
			}

			unset( $meta[$event_key][$delete_id] );

			update_user_meta( $user_id, $edition, $meta );

			return true;
		}

		static function render_form( $form_id ) {
			if ( ! $form_id ) {
				return ''; //self::generic_error_message();
			}

			ob_start();
			Ninja_Forms()->display( $form_id );

			return ob_get_clean();
		}

		static function render_restriction( $block_content, $attributes, $type = 'Sport specific activities' ) {
			if ( ! array_key_exists( 'align', $attributes ) ) {
				$attributes['align'] = 'full';
			}

			$user                = array_key_exists( 'userOnly', $attributes ) ? $attributes['userOnly'] : false;
			$restrictGlobalDate  = array_key_exists( 'restrictGlobal', $attributes ) ? $attributes['restrictGlobal'] : false;
			$restrictCustomStart = array_key_exists( 'restrictCustomStart', $attributes ) ? $attributes['dateStart'] : false;
			$restrictCustomEnd   = array_key_exists( 'restrictCustomEnd', $attributes ) ? $attributes['dateEnd'] : false;

			$text      = $attributes['timeRestrictionLabel'] ? $attributes['timeRestrictionLabel'] : false;
			$adminOnly = $attributes['adminOnly'] ? $attributes['adminOnly'] : false;

			$games_ended = self::games_ended();

			if ( $user && ! is_user_logged_in() ) {
				$register_link = \jg\Theme\HelpersTheme::theme_page( false, 'registration_start' )['permalink'];

				$block_content = self::cover_block( $classes, '<h4 class="text-center">This activity is for participants to complete. Make sure you <a class="btn-link" href="' . $register_link . '">register</a> and check back here to get started.</h4>' );
			}

			if ( $restrictGlobalDate ) {
				if ( $games_started ) {
					// Hide content as games not started based on global star date
					$block_content = $games_started;
				} else if ( $games_ended ) {
					// Hide content as games not ended based on global end date
					$block_content = $games_ended;
				}
			} else {
				$games_started = self::games_started( $text, $restrictCustomStart );
				$games_ended   = self::games_ended( false, $restrictCustomEnd );

				if ( $restrictCustomStart && $games_started ) {
					// Hide content as games not started based on custom start date
					$block_content = $games_started;
				} else if ( $restrictCustomEnd & $games_ended ) {
					// Hide content as games not ended based on custom end date
					$block_content = $games_ended;
				}
			}

			if ( $adminOnly && ! current_user_can( 'administrator' ) ) {
				if ( ! $games_started ) {
					$games_started = '<h4 class="text-center">These activities are currently unavailable.</h4>';
				}

				return self::cover_block( $classes, $games_started );
			}

			return $block_content;
		}

		// Delete post and user info if on delete_confirm page
		static public function should_delete( $user_id ) {
			$delete_type   = isset( $_GET['type'] ) ? $_GET['type'] : false;
			$delete_id     = isset( $_GET['id'] ) ? $_GET['id'] : false;
			$delete_action = isset( $_GET['action'] ) ? $_GET['action'] : false;
			$family        = \jg\Theme\HelpersUser::get_family( $user_id );

			if ( $delete_type && 'delete' === $delete_action && false !== $delete_id ) {
				return $delete_id;
			} else if ( $family && $delete_type && 'delete_confirm' === $delete_action && false !== $delete_id ) {
				global $post;
				$post_id      = $post->ID;
				$profile_page = \jg\Theme\HelpersTheme::theme_page( $post_id, 'profile' );
				$permalink    = $profile_page['permalink'] . '?type=' . $delete_type . $profile_page['append_query_user'];

				$new_points = get_user_meta( $user_id, 'points', true ) - $family[$delete_id]['points'];
				update_user_meta( $user_id, 'points', $new_points );

				self::remove_event( $user_id, $delete_id, 'attended_workshops' );
				self::remove_event( $user_id, $delete_id, 'registered_workshops' );
				self::remove_event( $user_id, $delete_id, 'completed_activities' );
				self::remove_event( $user_id, $delete_id, 'completed_training' );
				self::remove_event( $user_id, $delete_id, 'uploads' );

				unset( $family[$delete_id] );
				update_user_meta( $user_id, 'family', $family );

				echo '<script>location.href = \'' . $permalink . '\'</script>';
			}

			return false;
		}

		static function user_completion( $post_id, $user_id, $attributes, $complete_label = '' ) {
			if ( $user_id ) {
				$upload_type   = $attributes['uploadType'];
				$complete_type = $attributes['completeType'];
				$type          = $attributes['type'] ? $attributes['type'] : 'completed_training';
				$show_upload   = $attributes['uploadButton'] ? $attributes['uploadButton'] : false;
				$show_complete = $attributes['completeButton'] ? $attributes['completeButton'] : false;
				$form_id       = $attributes['nfUpload'];
				$id            = 'upload' . wp_rand();

				$post_id = trim( sanitize_title_with_dashes( $post_id ) );

				if ( $show_complete ) {
					$buttons .= \jg\Theme\HelpersUser::complete_by_user( $user_id, $post_id, '', $type, '', $complete_type );
				}

				if ( $show_upload && $form_id ) {
					if ( ! $upload_type ) {
						$upload_type = 'initial-fitness';
					}

					$label = str_replace( '-', ' ', ucwords( $upload_type, '_' ) ) . ' Results';

					$submit_label = 'Submit your ' . $label;

					$points = 0;
					if ( strpos( $upload_type, 'challenge' ) !== false ) {
						$points = (int) get_theme_mod( 'jg_points_challenges' );
					}

					$buttons = '<a class="btn btn-primary upload-button" data-bs-toggle="collapse" href="#' . $id . '" role="button" aria-expanded="false" aria-controls="' . $id . '">' . $submit_label . '</a>';
					$buttons = $buttons . '<div id="' . $id . '" class="collapse form-' . $form_id . '" data-points="' . $points . '" data-upload-type="' . $upload_type . '" data-upload-label="' . $label . '">' . self::render_form( $form_id ) . '</div>';

				}
			} else {
				$buttons = '<h4>Make sure you register to get started and don\'t forget to check back each week for your weekly training.</h4>';
			}

			return $buttons;
		}
	}

	new BlockHelpers();
}