<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockComplete::class ) ) {
	class BlockComplete {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'member-complete' );

			register_block_type( $block_json_file,
				[
					'render_callback' => [$this, 'render'],
				] );
		}

		function render( $attributes = [] ) {
			$helpersUser = new \jg\Theme\HelpersUser();
			$user_id     = $helpersUser->get_user_id();

			if ( $user_id ) {
				$complete_type = $attributes['completeType'];
				$type          = $attributes['type'] ? $attributes['type'] : 'completed_training';

				$title       = get_the_title();
				$slug        = str_replace( ['"', "'", '“', '”'], "", $title );
				$post_id     = get_the_ID();
				$activity_id = sanitize_title_with_dashes( $post_id . '-' . $slug );

				$edition = $helpersUser::get_edition();

				$edition_info = get_user_meta( $user_id, $edition, true );

				$buttons = \jg\Theme\HelpersUser::complete_by_user( $user_id, $activity_id, '', $type, '', $complete_type );
			} else {
				$buttons = '<h4>Make sure you register to get started and don\'t forget to check back each week for your weekly training.</h4>';
			}

			return '<div class="complete-button w-100 my-4">' . $buttons . '</div>';
		}
	}

	new BlockComplete();
}