<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockUpload::class ) ) {
	class BlockUpload {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'member-upload' );

			register_block_type( $block_json_file, [
				'render_callback' => [$this, 'render'],
			] );
		}

		function render( $attributes = [] ) {
			"\jg\Theme\HelpersUser() )->current_meta( $user_id )";
			return 'Upload';
		}
	}

	new BlockUpload();
}