<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockProfile::class ) ) {
	class BlockProfile {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		function get_dashboard( $user_id, $family ) {
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'profile' );

			register_block_type( $block_json_file,
				[
					'render_callback' => [$this, 'render'],
				] );
		}

		function render( $attributes ) {
			$helpersUser = new \jg\Theme\HelpersUser();
			$user_id     = $helpersUser->get_user_id();

			$admin_view_id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : false;
			$user_exists   = get_userdata( $admin_view_id );
			if ( current_user_can( 'administrator' ) && $admin_view_id && $user_exists ) {
				$user_id = $admin_view_id;
			}

			$user = new \WP_User( $user_id );

			$profile = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Login Required</h1>' );

			if ( $profile ) {
				return $profile;
			}

			if ( ! in_array( 'author', (array) $user->roles ) && ! current_user_can( 'administrator' ) ) {
				return BlockHelpers::cover_block( [], '<h1 class="mx-auto text-center mb-4">Error</h1><h3 class="mx-auto text-center">Please make sure that you have <u>clicked the link in your email</u> before going to your profile.</h3><h3 class="mx-auto text-center">If you have and you are seeing this message then please contact us for support.</h3>' );
			}
			$family = \jg\Theme\HelpersUser::get_family( $user_id );

			$profile_form  = BlockHelpers::render_form( $attributes['nfProfile'] );
			$password_form = BlockHelpers::render_form( $attributes['nfPassword'] );

			$family_tab = false;
			if ( is_array( $family ) ) {
				$family_tab = [
					'slug'    => 'family',
					'label'   => 'Family Members',
					'content' => ( new ProfileFamily() )->render( $user_id, $attributes['nfFamily'] ),
				];
			}

			$admin_tab = false;
			if ( current_user_can( 'administrator' ) ) {
				$users         = get_users();
				$profile_page  = \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'profile' )['permalink'];
				$admin_content = [];

				foreach ( $users as $user ) {
					$admin_user_id = $user->ID;

					$username     = $user->user_login;
					$account_type = get_user_meta( $admin_user_id, 'form_type', true );
					$family       = \jg\Theme\HelpersUser::get_family( $admin_user_id );

					$editions = get_user_meta( $admin_user_id, 'editions', true );

					foreach ( $editions as $edition ) {
						$edition_info = get_user_meta( $admin_user_id, $edition, true );

						if ( is_array( $edition_info ) ) {
							$meta_value = $edition_info['points'];
							$points     = ' - Points: ' . $meta_value;

							$meta_value    = $edition_info['uploads'];
							$meta_value    = is_array( $meta_value ) ? $meta_value : unserialize( $meta_value );
							$meta_value    = is_array( $meta_value ) ? $meta_value : [];
							$admin_uploads = '<br>Uploads: ' . count( $meta_value );

							$meta_value                 = $edition_info['attended_workshops'] ? $edition_info['attended_workshops'] : [];
							$admin_registered_workshops = '<br>Registered Workshops: ' . count( $meta_value );

							$meta_value               = $edition_info['registered_workshops'] ? $edition_info['registered_workshops'] : [];
							$admin_attended_workshops = ' - Attended Workshops: ' . count( $meta_value );

							$meta_value       = $edition_info['completed_activities'] ? $edition_info['completed_activities'] : [];
							$admin_activities = '<br>Activities: ' . count( $meta_value );

							$meta_value     = $edition_info['completed_training'] ? $edition_info['completed_training'] : [];
							$admin_training = ' - Trainings: ' . count( $meta_value );

							$meta_value       = $edition_info['challenges'] ? $edition_info['challenges'] : [];
							$admin_challenges = ' - Challenges: ' . count( $meta_value );

							$meta_value         = $edition_info['bonus_points'] ? $edition_info['bonus_points'] : [];
							$admin_bonus_points = '<br>Bonus Points: ' . count( $meta_value );

							$admin_members = [];
							$children      = '';
							if ( $family ) {
								foreach ( $family as $member ) {
									if ( is_array( $member ) ) {
										$admin_members[] = $member['first_name'] . ' ' . $member['last_name'];
									}
								}

								$children = '<br><br><b>Children:</b>' . '<br>' . implode( '<br>', $admin_members );
							}

							$admin_content[$edition][$account_type][$admin_user_id] = '<a href="' . $profile_page . '?user_id=' . $admin_user_id . '">' . $admin_user_id . ' - ' . esc_html( $username ) . ', ' . esc_html( $user->user_email ) . '</a>' . $points . $admin_uploads . $admin_registered_workshops . $admin_attended_workshops . $admin_activities . $admin_training . $admin_challenges . $admin_bonus_points . $children . '<br>';
						} else {
							$admin_content[$edition][$account_type][$admin_user_id] = '<a href="' . $profile_page . '?user_id=' . $admin_user_id . '">' . $admin_user_id . ' - ' . esc_html( $username ) . ', ' . esc_html( $user->user_email ) . '</a>';
						}
					}
				}

				$edition_content = [];
				foreach ( $editions as $edition ) {
					$edition_content[$edition] = '<h2>Edition: ' . $edition . '</h2><br><h3>Parents:</h3>' . implode( '<br>', $admin_content[$edition]['parent'] ) . '<br><br><h3>Youth:</h3>' . implode( '<br>', $admin_content[$edition]['youth'] );
				}

				$admin_tab = [
					'slug'    => 'admin',
					'label'   => 'Choose User (admin only)',
					'content' => implode( $edition_content ),
				];
			}

			$links = [$admin_tab, [
				'slug'    => 'dashboard',
				'label'   => 'Dashboard',
				'content' => ( new ProfileDashboard() )->render( $user_id, $family ),
			], [
				'slug'    => 'profile',
				'label'   => 'Update Profile',
				'content' => $profile_form,
			], [
				'slug'    => 'password',
				'label'   => 'Update Password',
				'content' => $password_form,
			], $family_tab];

			$classes = BlockHelpers::gutenberg_classes( ['profile'], $attributes );

			return BlockHelpers::cover_block( $classes, $this->tabs( $links ) );
		}

		function tabs( $links ) {
			$tabs     = [];
			$contents = [];

			$modify_type = $_GET['type'] ? $_GET['type'] : 'dashboard';
			$modify_id   = isset( $_GET['id'] ) ? $_GET['id'] : false;
			if ( 'family' === $modify_type && 'profile' === $modify_id ) {
				$modify_type = 'profile';
			}

			foreach ( $links as $link ) {
				if ( ! $link ) {
					continue;
				}

				$slug = $link['slug'];

				$nav_classes     = [$slug, 'nav-link'];
				$content_classes = [$slug, 'tab-pane', 'fade'];

				if ( $modify_type === $slug ) {
					$nav_classes[]     = 'active';
					$content_classes[] = 'active';
					$content_classes[] = 'show';
				}

				$tabs[] = '<a class="' . implode( ' ', $nav_classes ) . '" id="v-pills-' . $slug . '-tab" data-bs-toggle="pill" href="#v-pills-' . $slug . '" role="tab" aria-controls="v-pills-' . $slug . '" aria-selected="true">' . $link['label'] . '</a>';

				$contents[] = '<div id="v-pills-' . $slug . '" class="' . implode( ' ', $content_classes ) . '" role="tabpanel" aria-labelledby="v-pills-' . $slug . '-tab">' . $link['content'] . '</div>';
			}

			$tabs     = '<div id="v-pills-tab" class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">' . implode( $tabs ) . '</div>';
			$contents = '<div class="tab-content" id="v-pills-userTabs">' . implode( $contents ) . '</div>';

			return '<div class="d-flex align-items-start">' . $tabs . $contents . '</div>';
		}
	}

	new BlockProfile();
}