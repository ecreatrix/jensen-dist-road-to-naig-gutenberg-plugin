<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( ProfileDashboard::class ) ) {
	class ProfileDashboard {
		public function meta_activity( $meta, $key ) {
			$all_activity = $this->meta_value( $meta, $key );
			if ( ! $all_activity || ! is_array( $all_activity ) ) {
				return false;
			}

			$completed_activity = [];
			foreach ( $all_activity as $event_id => $Points ) {
				$activity_info = explode( '-', $event_id );
				$id            = array_shift( $activity_info );
				$activity      = get_post( $id );
				$slug          = implode( '-', $activity_info );
				$key           = sanitize_title_with_dashes( $activity->post_title . '-' . $slug );

				$title = $activity->post_title . ' - ' . implode( ' ', $activity_info );

				$completed_activity[$key] = '<a class="d-block activity post-' . $id . '" href="' . get_permalink( $id ) . '?block=' . $slug . '#' . $slug . '">' . $title . '</a>';
			}

			ksort( $completed_activity );
			return '<div class="activity"><h4 class="mt-4">Activity</h4>' . implode( $completed_activity ) . '</div>';
		}

		public function meta_bonus_points( $meta, $key ) {
			$awarded_points = $this->meta_value( $meta, $key );
			if ( ! $awarded_points || ! is_array( $awarded_points ) ) {
				return false;
			}

			$bonus_points = [];
			foreach ( $awarded_points as $points ) {
				$bonus_points[] = '<div class="points">' . $points['description'] . ' - ' . $points['points'] . '</div>';
			}

			return '<div class="bonus-points"><h4 class="mt-4">Bonus Points</h4>' . implode( $bonus_points ) . '</div>';
		}

		public function meta_challenges( $meta, $key ) {
			$all_challenges = $this->meta_value( $meta, $key );
			if ( ! $all_challenges || ! is_array( $all_challenges ) ) {
				return false;
			}

			$challenges = [];
			foreach ( $all_challenges as $event_id => $Points ) {
				$challenges_info = explode( '-', $event_id );
				$id              = array_shift( $challenges_info );
				$challenges      = get_post( $id );
				$slug            = implode( '-', $challenges_info );
				$key             = sanitize_title_with_dashes( $challenges->post_title . '-' . $slug );
				unset( $challenges_info[0] );

				$title = $challenges->post_title . ' - ' . implode( ' ', $challenges_info );

				$challenges[$key] = '<a class="d-block challenges post-' . $id . '" href="' . get_permalink( $id ) . '?block=' . $slug . '#' . $slug . '">' . $title . '</a>';
			}

			ksort( $cchallenges );
			return '<div class="challenges"><h4 class="mt-4">Challenges</h4>' . implode( $challenges ) . '</div>';
		}

		public function meta_training( $meta, $key ) {
			$all_training = $this->meta_value( $meta, $key );
			if ( ! $all_training || ! is_array( $all_training ) ) {
				return false;
			}

			$completed_training = [];
			foreach ( $all_training as $event_id => $training ) {
				$training_info = explode( '-', $event_id );
				$id            = array_shift( $training_info );
				$training_post = get_post( $id );
				$slug          = implode( '-', $training_info );
				$key           = sanitize_title_with_dashes( $training_post->post_title . '-' . $slug );

				if ( is_array( $training ) ) {
					$title = $training_post->post_title . ' - ' . $training['label'];
				} else {
					unset( $training_info[0] );
					$title = $training_post->post_title . ' - ' . implode( ' ', $training_info );
				}

				$completed_training[$key] = '<a class="d-block training post-' . $id . '" href="' . get_permalink( $id ) . '?block=' . $slug . '#' . $slug . '">' . $title . '</a>';
			}

			ksort( $completed_training );
			return '<div class="training"><h4 class="mt-4">Training</h4>' . implode( $completed_training ) . '</div>';
		}

		public function meta_uploads( $meta, $key ) {
			$upload_ids = $this->meta_value( $meta, $key );

			if ( ! $upload_ids || ! is_array( $upload_ids ) ) {
				return false;
			}

			$uploads = [];
			foreach ( $upload_ids as $key => $upload ) {
				if ( is_int( $upload ) ) {
					$title = ucwords( str_replace( '-', ' ', $key ) );
					if ( 'Fitness Initial' === $title ) {
						$title = 'Initial Fitness Test';
					} else if ( 'Fitness Final' === $title ) {
						$title = 'Final Fitness Test';
					}

					$uploads[] = '<div class="upload post-' . $id . '"><a href="' . wp_get_attachment_url( $upload ) . '">' . $title . '</a></div>';
				} else {
					$post_id   = $upload['post'];
					$url       = $upload['url'];
					$post_name = get_the_title( $post_id );
					$title     = ucwords( $post_name . ' - ' . str_replace( '-', ' ', $upload['upload_type'] ) );

					$uploads[] = '<div class="upload post-' . $id . '"><a href="' . $url . '" target="_blank">' . $title . '</a></div>';
				}
			}

			return '<div class="uploads"><h4 class="mt-4">Uploads</h4>' . implode( $uploads ) . '</div>';
		}

		public function meta_value( $meta, $key ) {
			$values = is_array( $meta ) && array_key_exists( $key, $meta ) ? $meta[$key] : false;

			$values = $values && $values[0] ? $values[0] : $values;

			return is_array( $values ) ? $values : unserialize( $values );
		}

		public function meta_workshops( $meta, $key, $type = 'Registered' ) {
			$workshop_ids = $this->meta_value( $meta, $key );
			if ( ! $workshop_ids || ! is_array( $workshop_ids ) ) {
				return false;
			}

			$workshops = [];
			foreach ( $workshop_ids as $id => $points ) {
				$workshop  = get_post( $id );
				$date_meta = strtotime( str_replace( '/', '-', get_field( 'jg_timedate', $id ) ) );

				$date = date( 'jS F', $date_meta );

				$workshops[$date_meta] = '<a class="d-block workshop post-' . $id . '" href="' . get_permalink( $id ) . '">' . $workshop->post_title . ' - Date: ' . $date . ', Time: ' . date( 'H:i A', $date_meta ) . '</a>';
			}

			ksort( $workshops );
			return '<div class="workshops"><h4 class="mt-4">' . $type . ' Workshops</h4>' . implode( $workshops ) . '</div>';
		}

		public function render( $user_id, $form_id ) {
			$edition  = \jg\Theme\HelpersUser::get_edition();
			$editions = get_user_meta( $user_id, 'editions', true );

			if ( ! in_array( $edition, $editions ) ) {
				// Add current season to account if not there yet
				$editions[] = $edition;
				$editions   = array_unique( $editions );
				update_user_meta( $user_id, 'editions', $editions );
			}

			if ( ! $editions || ! is_array( $editions ) ) {
				$new_editions = [$edition];
				$editions     = $editions && is_array( $editions ) ? array_merge( $editions, $new_editions ) : $new_editions;
			}

			$content = [];
			foreach ( $editions as $edition ) {
				$edition_info = get_user_meta( $user_id, $edition, true );
				if ( ! is_array( $edition_info ) || empty( $edition_info ) ) {
					$edition_info = array_merge( \jg\Theme\HelpersUser::member_info(), [] );
					update_user_meta( $user_id, $edition, $edition_info );
					var_dump( $edition_info );
				}

				$name      = '<h2>' . ucfirst( str_replace( '_', ' ', $edition ) ) . '</h2>';
				$content[] = $name . $this->render_edition( $user_id, $form_id, $edition );
			}

			return implode( '<hr>', array_reverse( $content ) );
		}

		public function render_edition( $user_id, $form_id, $edition ) {
			$family       = \jg\Theme\HelpersUser::get_family( $user_id );
			$edition_info = get_user_meta( $user_id, $edition, true );

			$points = is_array( $edition_info ) && array_key_exists( 'points', $edition_info ) ? $edition_info['points'] : 0;

			$members = '';
			if ( $family ) {
				$members    = [];
				$workshops  = [];
				$training   = [];
				$challenges = [];

				foreach ( $family as $key => $member ) {
					if ( is_array( $member ) ) {
						$member_meta = $member[$edition];

						$member_name = $member['first_name'];

						$points = $member[$edition] && array_key_exists( 'points', $member[$edition] ) ? $member[$edition]['points'] : 0;

						$intro = '<div class="intro">' . $this->user_intro( $member_name, $points ) . '</div>';

						if ( $member_meta ) {
							$members[$key] = '<div class="mt-5 ' . $key . '">' . $intro . $this->render_edition_member( $member_meta ) . '</div>';
						}
					}
				}

				$members = count( $members ) > 0 ? '<div class="members pb-4">' . implode( $members ) . '</div>' : '';

				$name    = $this->user_intro( 'Entire Family', $points );
				$content = $name . $members;
			} else {
				$user = get_user_by( 'id', $user_id );
				$name = $this->user_intro( $user->first_name, $points );

				$content = $name . $this->render_edition_member( $edition_info );
			}

			return $content;
		}

		public function render_edition_member( $meta ) {
			$registered_workshops = $this->meta_workshops( $meta, 'registered_workshops' );

			$attended_workshops = $this->meta_workshops( $meta, 'attended_workshops', 'Attended' );

			$completed_training = $this->meta_training( $meta, 'completed_training' );

			$challenges = $this->meta_challenges( $meta, 'challenges' );

			$activities = $this->meta_activity( $meta, 'completed_activities' );

			$uploads = $this->meta_uploads( $meta, 'uploads' );

			$bonus_points = $this->meta_bonus_points( $meta, 'bonus_points' );

			return $uploads . $registered_workshops . $attended_workshops . $challenges . $completed_training . $activities . $bonus_points;
		}

		public function user_intro( $first_name, $points ) {
			$points = $points ? $points : 0;

			return '<h3 class="d-inline">' . $first_name . '</h3><span class="points"> - ' . $points . ' points</span>';
		}
	}
}