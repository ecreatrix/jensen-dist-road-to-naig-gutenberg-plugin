<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( ProfileFamily::class ) ) {
	class ProfileFamily {
		function render( $user_id, $form_id ) {
			$label = BlockHelpers::get_action_label( 'family', 'family member' );

			$form          = BlockHelpers::render_form( $form_id );
			$should_delete = BlockHelpers::buttons_delete( 'family', $user_id );
			if ( $should_delete ) {
				return $should_delete;
			}

			/*$games_ended = BlockHelpers::games_ended();
			if ( $games_ended ) {
			$form = $games_ended;
			}*/

			$table = $this->table( $user_id );

			return $table . $label . $form;
		}

		function table( $user_id ) {
			$family = \jg\Theme\HelpersUser::get_family( $user_id );

			if ( ! $family ) {
				return '';
			}

			$family = array_merge( [
				'profile' => (
					new \jg\Theme\HelpersUser() )->current_meta( $user_id ),
			], $family
			);

			$members = [];

			foreach ( $family as $key => $member ) {
				if ( ! $key ) {
					continue;
				}

				$members[$key] = $this->table_row_member( $key, $member );
			}

			//return '<div class="table-responsive"><table class="table table-striped table-hover"><thead><tr><th>Name</th><th>Age</th><th>Shirt Size</th><th>Completed Activities</th><th>Registered Workshops</th><th></th></tr></thead><tbody>'
			return '<div class="table-responsive"><table class="table table-striped table-hover"><thead><tr><th>Name</th><th>Age</th><th>Shirt Size</th><th></th></tr></thead><tbody>'
			. implode( $members )
				. '</tbody></table></div>';
		}

		function table_row_member( $key, $member ) {
			$member_name  = '<td>' . trim( $member['first_name'] . ' ' . $member['last_name'] ) . '</td>';
			$age_category = '<td>' . $member['age'] . '</td>';

			if ( 'profile' === $key ) {
				$shirt_size = '<td>N/A</td>';
			} else {
				$shirt_size = '<td>' . ucwords( str_replace( '-', ' ', $member['shirt_size'] ) ) . '</td>';
			}

			/*$sports = [];
			foreach ( $member['completed_activities'] as $sportID ) {
			$sports[] = BlockHelpers::get_sport_info( $sportID );
			}
			$sports = '<td>' . implode( '<br>', $sports ) . '</td>';

			$workshops = [];
			foreach ( $member['registered_workshops'] as $sportID ) {
			$workshops[] = BlockHelpers::get_workshop_info( $sportID );
			}
			$workshops = '<td>' . implode( '<br>', $workshops ) . '</td>';*/

			//$buttons = '<td>' . BlockHelpers::buttons_change( 'family', $key ) . '</td>';

			return '<tr class="family id-' . $key . '">' . $member_name . $points . $age_category . $shirt_size . $sports . $workshops . $buttons . '</tr>';
		}
	}
}