<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlocksPostGrid::class ) ) {
	class BlocksPostGrid {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'post-grid' );

			register_block_type( $block_json_file,
				[
					'render_callback' => [$this, 'render'],
				] );
		}

		function render( $attributes ) {
			$args = [
				'post_type'      => $attributes['postType'],
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'orderby'        => 'ID',
				'order'          => 'ASC',
			];
			$posts = get_posts( $args );

			if ( empty( $posts ) ) {
				return BlockHelpers::cover_block( '', '<h4 class="text-center py-5">No upcoming posts available</h4>', );
			}

			$single = [];
			foreach ( $posts as $post ) {
				$id       = $post->ID;
				$datetime = BlockHelpers::post_datetime( $id );

				$key = $datetime['key'];

				$single[$key] = $this->render_single( $id, $datetime['content'] );
			}

			ksort( $single );

			$content = '<div class="row">' . implode( $single ) . '</div>';

			return BlockHelpers::cover_block( ['posts-grid'], $content );
		}

		function render_single( $id, $datetime ) {
			$thumb     = get_the_post_thumbnail( $id, 'large' );
			$classes   = ['post', 'post-' . $id, 'col-12 col-sm-6 col-md-4'];
			$permalink = get_permalink( $id );

			if ( $thumb ) {
				$block = '<a class="image" href="' . $permalink . '">' . $thumb . '</a>';
			} else {
				$title = '<span class="title">' . get_the_title( $id ) . '</span>';

				$block = '<a class="text" href="' . $permalink . '">' . $title . $datetime . '</a>';
			}

			return '<div class="' . implode( ' ', $classes ) . '">' . $block . '</div>';
		}
	}

	new BlocksPostGrid();
}