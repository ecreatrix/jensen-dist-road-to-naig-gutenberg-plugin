<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlocksPostCarousel::class ) ) {
	class BlocksPostCarousel {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'post-carousel' );

			register_block_type( $block_json_file,
				[
					'render_callback' => [$this, 'render'],
				] );
		}

		function render( $attributes ) {
			$limit = $attributes['limit'];

			$args = [
				'post_type'      => $attributes['postType'],
				'post_status'    => 'publish',
				'posts_per_page' => -1,
				'orderby'        => 'ID',
				'order'          => 'ASC',
			];
			$posts = get_posts( $args );

			if ( empty( $posts ) ) {
				return BlockHelpers::cover_block( '', '<h4 class="text-center py-5">No upcoming posts available</h4>', );
			}
			$id = 'post-carousel-' . wp_rand();

			$slides     = [];
			$indicators = [];

			$i = 0;
			foreach ( $posts as $post ) {
				if ( $i === $limit ) {
					break;
				}

				$post_id = $post->ID;

				$datetime = BlockHelpers::post_datetime( $post_id );
				$key      = $datetime['key'];

				$slides[$key]   = self::render_single_slide( $i, $post, $datetime['content'] );
				$indicators[$i] = self::render_single_indicator( $i, $id, $post_id );

				$i++;
			}

			array_pop( $indicators );

			ksort( $slides );

			$indicators = '<div class="carousel-indicators">' . implode( $indicators ) . '</div>';

			$slides = '<div class="carousel-inner">' . implode( $slides ) . '</div>';

			$controls = '<button class="carousel-control-prev" type="button" data-bs-target="#' . $id . '" data-bs-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="visually-hidden">Previous</span></button><button class="carousel-control-next" type="button" data-bs-target="#' . $id . '" data-bs-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="visually-hidden">Next</span></button>';

			$content = '<div id="' . $id . '" class="carousel slide carousel-fade" data-bs-ride="carousel">' . $indicators . $slides . $controls;

			return BlockHelpers::cover_block( ['posts-carousel'], $content ) . '</div>';
		}

		static function render_single_indicator( $i, $id, $post_id ) {
			$indicator_classes = [$post_id];

			if ( 0 === $i ) {
				$indicator_classes[] = 'active';
			}

			return '<button class="' . implode( ' ', $indicator_classes ) . '" type="button" data-bs-target="#' . $id . '" data-bs-slide-to="' . $i . '" aria-current="true" aria-label="Post"></button>';
		}

		static function render_single_slide( $i, $post, $datetime ) {
			$post_id       = $post->ID;
			$slide_classes = ['carousel-item', 'id-' . $post_id];

			if ( 0 === $i ) {
				$slide_classes[] = 'active';
			}

			$thumb = get_the_post_thumbnail( $post_id, 'large' );
			if ( $thumb ) {
				$content = $thumb;

				$slide_classes[] = 'image';
			} else {
				$name = '<h5 class="name">' . $post->post_title . '</h5>';

				$excerpt = $post->post_content;
				if ( $excerpt ) {
					$excerpt = wp_trim_words( apply_filters( 'the_content', $excerpt ), 5, '' );
					$excerpt = '<div class="excerpt">' . $excerpt . '</div>';
				}

				$content = $name . $excerpt . $datetime;

				$slide_classes[] = 'text';
			}

			$content = '<a class="btn-link" href="' . get_permalink( $post_id ) . '">' . $content . '</a>';

			return '<div class="' . implode( ' ', $slide_classes ) . '" data-bs-interval="4000">' . $content . '</div>';
		}
	}

	new BlocksPostCarousel();
}