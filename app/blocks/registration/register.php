<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockRegistration::class ) ) {
	class BlockRegistration {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'register' );

			register_block_type( $block_json_file,
				[
					'render_callback' => [$this, 'render'],
				] );
		}

		function render( $attributes ) {
			$registered = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Already logged-in</h1>' );
			if ( $registered ) {
				return $registered;
			}

			$form = BlockHelpers::render_form( $attributes['nfRegistration'] );

			$classes = BlockHelpers::gutenberg_classes( ['register'], $attributes );

			return BlockHelpers::cover_block( $classes, $form );
		}
	}

	new BlockRegistration();
}