<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockFinishRetistration::class ) ) {
	class BlockFinishRetistration {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
		}

		public function finish_registration( $form_id, $user_submission_id ) {
			$form             = Ninja_Forms()->form( $form_id );
			$form_submissions = $form->get_subs();

			$actions   = Ninja_Forms()->form( $form_id )->get_actions();
			$field_ids = false;

			// Ninja form not found
			if ( ! is_array( $form_submissions ) || empty( $form_submissions ) ) {
				return false;
			}

			if ( 'reset' === $user_submission_id || 'resend' === $user_submission_id ) {
				$current_user = wp_get_current_user();
				if ( is_user_logged_in() && ! in_array( 'administrator', (array) $current_user->roles ) ) {
					return false;
				}
			}

			$action_keys    = [];
			$email_settings = false;
			foreach ( $actions as $action ) {
				$action_type = $action->get_setting( 'type' );
				$settings    = $action->get_settings();

				if ( 'jg-register-user' === $action_type ) {
					$action_keys = \jg\Plugin\User\NinjaHelpersFields::strip_merge_tags( $settings );
				} else if ( 'email' === $action_type ) {
					$email_settings = $settings;
				}
			}

			if ( ! $action_keys ) {
				return false;
			}

			// Using get_sub([id]) doesn't work
			foreach ( $form_submissions as $submission ) {
				$fields = $submission->get_field_values();
				$seq    = $submission->get_seq_num();

				$email     = $fields[$action_keys['email']];
				$password  = $fields[$action_keys['new_password']];
				$username  = $fields[$action_keys['username']];
				$form_type = $fields[$action_keys['form_type']];

				$user = get_user_by( 'email', $email );

				if ( 'reset' === $user_submission_id && $user ) {
					( new \jg\Theme\HelpersUser() )->refresh_users( $fields, $action_keys, $user );
				} else if ( 'resend' === $user_submission_id && $seq >= 380 && $seq <= 416 ) {
					$message = $email_settings['email_message'];
					$message = str_replace( '{submission:sequence}', $seq, $message );
					$subject = $email_settings['email_subject'];

					//$status = wp_mail( $email, $subject, $message );
				} else if ( $seq == $user_submission_id ) {
					if ( ! $user ) {
						return false;
					}

					if ( ! in_array( 'administrator', (array) $user->roles ) ) {
						$user->set_role( 'author' );
					}
				}
			}

			return true;
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'set-user-data' );

			register_block_type( $block_json_file,
				[
					'render_callback' => [$this, 'render'],
				] );
		}

		function render( $attributes ) {
			$user_submission_id = $_GET['id'];
			$form_id            = isset( $attributes['nfRegistration'] ) ? $attributes['nfRegistration'] : 10;

			$error = BlockHelpers::cover_block( [], '<h1 class="mx-auto text-center mb-4">Error</h1><h3 class="mx-auto text-center">Please contact us for support.</h3>' );

			if ( ! $user_submission_id || ! isset( $user_submission_id ) || ! $form_id ) {
				// Submission ID/Form ID missing
				return $error;
			}

			$loggedIn = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Already logged-in</h1>' );
			if ( 'reset' !== $user_submission_id && $loggedIn ) {
				//return $loggedIn;
			}

			// On registration page but Ninja Forms not active
			if ( ! class_exists( 'Ninja_Forms' ) ) {
				return $error;
			}

			$finish = $this->finish_registration( $form_id, $user_submission_id );

			if ( ! $finish ) {
				return $error;
			}

			return BlockHelpers::cover_block( [], '<h1 class="mx-auto text-center mb-4">Thank you for registering</h1><h3 class="mx-auto text-center"><a href="' . get_permalink( get_theme_mod( 'jg_signin' ) ) . '" style="color: inherit; text-decoration: underline">Log in here</a></h3>' );
		}
	}

	new BlockFinishRetistration();
}