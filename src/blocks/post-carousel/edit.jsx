// External dependencies
import { isUndefined, pickBy } from 'lodash'

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    InnerBlocks,
    InspectorControls,
    withColors,
    PanelColorSettings,
} = wp.blockEditor

const {
    PanelBody, 
    SelectControl, 
    ServerSideRender,
    Disabled
} = wp.components

const { useSelect } = wp.data

const { store: coreStore } = wp.coreData

// Other Dependencies

// Internal dependencies

function PostCarousel( { 
    setAttributes,
    attributes,
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    className
} ) {
    const { numSlides, postType } = attributes

    const { posts, selectTypes } = useSelect(select => {
        const {
            getEntityRecords,
            isResolving,
            hasFinishedResolution,
            getPostTypes
        } = select( coreStore );

        const postsParameters = [
            'postType',  postType,
            {
                per_page: numSlides,
            },
        ]
        const hasResolvedPosts = hasFinishedResolution(
            'getEntityRecords',
            postsParameters
        )
        const posts = getEntityRecords( ...postsParameters ) || null

        let selectTypes =  [{ value: false, label: 'Select Type' }]
        const allTypes = getPostTypes();
        const hasResolvedTypes = hasFinishedResolution(
            'getPostTypes',
        )
        hasResolvedTypes && allTypes.map( ( type, i ) => {
            if( type.slug !== 'attachment' && type.slug !== 'wp_block' && type.slug !== 'wp_template' ) {
                selectTypes = [ ...selectTypes, { value: type.slug, label: type.labels.name } ]
            }
        } )

        return {
            selectTypes,
            posts
        }
    },[
        numSlides, postType,
    ])

    let inspectorControls = <>
        <InspectorControls key="inspector">
            <PanelColorSettings
                title={ __( 'Background Color' ) }
                initialOpen={ true }
                colorSettings={ [
                    {
                        value: textColor.color,
                        onChange: setTextColor,
                        label: __( 'Text color' ),
                    },
                    {
                        value: backgroundColor.color,
                        onChange: setBackgroundColor,
                        label: __( 'Background color' ),
                    },
                ] }
            >
            </PanelColorSettings>
            <PanelBody title={ __( 'Content' ) }>
                <SelectControl
                    label={ __( 'Select Post Type' ) }
                    value={ postType }
                    options={ selectTypes }
                    onChange={ ( value ) => setAttributes( { postType: value } ) }
                />
            </PanelBody>
        </InspectorControls>
    </>

    return (
        <>
            { inspectorControls }
            <Disabled>
                <ServerSideRender
                    block="jg/post-carousel"
                    attributes={ attributes }
                />
            </Disabled>
        </>
    )
}

export default compose(
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( PostCarousel )
