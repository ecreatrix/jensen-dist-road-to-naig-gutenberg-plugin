// External dependencies
import classnames from 'classnames'

// Wordpress dependencies
const {
    InnerBlocks,
} = wp.blockEditor

// Internal dependencies
export default function save( props ) {
    const {
        attributes,
        className
    } = props

    const {
        backgroundColor,
        textColor,
        title,
    } = attributes

    const style = {
        backgroundColor: backgroundColor && backgroundColor.color,
        textColor: textColor && textColor.color,
    }

    const classes = classnames( 
        'wp-block-cover', 'page-heading', className, 
        backgroundColor && `has-${ backgroundColor }-background-color has-background-dim`,
        textColor && `has-${ textColor }-color has-${ textColor }-text-color`,
    )

    return (
        <div className={ classes } style={ style }><div className="wp-block-cover__inner-container"><h1 className="entry-title">{ title }</h1></div></div>
    )
}
