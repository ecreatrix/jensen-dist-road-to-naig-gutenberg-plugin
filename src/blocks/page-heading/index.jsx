// External dependencies

// WordPress dependencies
const {
    __,
} = wp.i18n

// Internal dependencies
import metadata from './block.json'
import edit from './edit'
import save from './save'

const settings = {
    icon: <svg viewBox="0 0 16 10" xmlns="http://www.w3.org/2000/svg"><rect fill="#000000" x="0" y="0.55" width="16" height="8.7"></rect><path d="M10.029801,2.47339585 L7.46584183,2.47339585 L7.46584183,2.44287255 L7.19113192,2.50391915 C6.4890955,2.62601245 5.97019902,3.23647885 5.97019902,3.96903865 C5.97019902,4.70159835 6.4890955,5.31206485 7.19113192,5.43415815 L7.40479518,5.46468145 L7.40479518,7.35712745 L7.86264502,7.35712745 L7.86264502,2.93124565 L8.74782137,2.93124565 L8.74782137,7.35712745 L9.20567121,7.35712745 L9.20567121,2.93124565 L10.029801,2.93124565 L10.029801,2.47339585 Z" fill="#FFFFFF"></path></svg>,
    edit,
    save,
}

export default {
    metadata,
    settings
}