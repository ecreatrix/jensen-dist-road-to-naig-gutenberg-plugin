// External dependencies
import classnames from 'classnames'

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    Component,
    Fragment
} = wp.element

const {
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

const {
    Placeholder,
    Spinner,
} = wp.components

import { pin } from '@wordpress/icons'

const {
    withSelect,
    select,
} = wp.data

// Other Dependencies

// Internal dependencies
function PageHeading( { 
    isRequesting,
    setAttributes,
    attributes: { title },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    className,
    pageTitle
} ) {
    let inspectorControls = <>
        <InspectorControls key="inspector">
        <PanelColorSettings
            title={ __( 'Background Color' ) }
            initialOpen={ true }
            colorSettings={ [
                {
                    value: textColor.color,
                    onChange: setTextColor,
                    label: __( 'Text color' ),
                },
                {
                    value: backgroundColor.color,
                    onChange: setBackgroundColor,
                    label: __( 'Background color' ),
                },
            ] }
        >
        </PanelColorSettings>
        </InspectorControls>
    </>

    if ( isRequesting ) {
        return (
            <>
                { inspectorControls }
                <Placeholder icon={ pin } label={ __( 'Activity Heading' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    if( pageTitle !== title ) {
        setAttributes( { title: pageTitle } )
    }

    const classes = classnames( 
        'wp-block-cover', 'page-heading', 'has-background-dim', 
        className, 
        {
            [ backgroundColor.class ]: backgroundColor.class,
            [ textColor.class ]: textColor.class,
        } 
    )

    const blockProps = useBlockProps( {
        className: classes,
        style: { backgroundColor: backgroundColor && backgroundColor.color },
    } ) 

    return (
        <>
            { inspectorControls }
            <div { ...blockProps }><div className="wp-block-cover__inner-container"><h1 className="entry-title">{ title }</h1></div></div>
        </>
    )
}

export default compose(
    withSelect( ( select, props ) => {
        const pageTitle = select("core/editor").getEditedPostAttribute('title')

        return {
            pageTitle
        }
    } ),

    withColors( 'backgroundColor', 'textColor' ),
)( PageHeading )
