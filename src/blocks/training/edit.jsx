// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    useEffect
} = wp.element

const {
    InnerBlocks,
    useBlockProps,
    InspectorControls,
    __experimentalUseInnerBlocksProps: useInnerBlocksProps,
} = wp.blockEditor

const {
    PanelBody, 
    TextControl,
    TextareaControl,
    ToggleControl,
    RadioControl
} = wp.components

// Other Dependencies

// Internal dependencies
import { datePanel } from '../components/date'
import { buttonsControl } from '../components/buttons'

const ALLOWED_BLOCKS = [ 'core/column', 'core/paragraph', 'core/heading', 'core/list', 'core/group', 'core/separator', 'core/spacer', 'core/buttons', 'core/image', 'core/video', 'jg/resource', 'jg/training-subblock' ];

export default function Training( { 
        attributes,
        attributes: { blockId, title, type },
        setAttributes,

        clientId,
        className,
} ) {

    useEffect(() => {
        //This conditional is useful to only set the id attribute once when the component mounts for the first time
        let tempBlockId = `block-${ clientId.split('-')[0] }`

        blockId !== tempBlockId && setAttributes( { blockId: tempBlockId } )
    }, [])

    let inspectorControls = <>
        <InspectorControls key="inspector">
            <PanelBody title={ __( 'User/Time Restrictions' ) }>
                { datePanel( attributes, setAttributes ) }
            </PanelBody>
            <PanelBody title={ __( 'Complete/upload buttons' ) }>
                { buttonsControl( attributes, setAttributes ) }
            </PanelBody>
            <PanelBody title={ __( 'Content' ) }>
                <RadioControl
                    label="Type"
                    selected={ type }
                    options={ [
                        { label: 'Activity', value: 'completed_activities' },
                        { label: 'Training', value: 'completed_training' },
                    ] }
                    onChange={ ( value ) => setAttributes( { type: value } ) }
                />
            </PanelBody>
        </InspectorControls>
    </>

    const blockProps = useBlockProps( {
        className
    } ) 

    const innerBlocksProps = useInnerBlocksProps( blockProps, {
        allowedBlocks: ALLOWED_BLOCKS,
        template: [
            [
                'core/paragraph',
                {
                    placeholder: __( 'Add Content...' ),
                },
            ],
        ]
    } )

    return (
        <>
            { inspectorControls }
            
            <div { ...blockProps } >
                <div className="wp-block-cover__inner-container">
                    <TextControl
                        label={ __( 'Accordion Title' ) }
                        onChange={ ( value ) => setAttributes( { title: value } ) }
                        value={ title }
                    />
                    <div { ...innerBlocksProps } />
                </div>
            </div>
        </>
    )
}