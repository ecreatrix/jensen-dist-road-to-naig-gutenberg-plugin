// External dependencies

// WordPress dependencies

// Internal dependencies
import metadata from './block.json'
import edit from './edit'
import save from './save'

const settings = {
    edit,
    save,
}

export default {
    metadata,
    settings
}