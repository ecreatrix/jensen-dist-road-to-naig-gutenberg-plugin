// External dependencies
import classnames from 'classnames/dedupe'

// Internal dependencies
const { __ } = wp.i18n

const {
    InnerBlocks,
} = wp.blockEditor

// Internal dependencies
import { coloursProps } from '../components/props'

export default function save( props ) {
    const allColoursProps = coloursProps(props, [], true)

    return <div 
        className={ allColoursProps.classes }
        style={ { ...allColoursProps.style } }
    >
       Complete Button
    </div>
}
