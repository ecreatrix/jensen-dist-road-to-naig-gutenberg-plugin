// External dependencies

// WordPress dependencies

const {
    ServerSideRender,
    Disabled
} = wp.components

// Other Dependencies

// Internal dependencies
function MemberComplete( props ) {
    return (
        <>
            <Disabled>
                Complete button for user
            </Disabled>
        </>
    )
}

export default MemberComplete 