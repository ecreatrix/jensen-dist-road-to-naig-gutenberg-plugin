// External dependencies

// WordPress dependencies

// Internal dependencies
import edit from './edit'
import metadata from './block'

const settings = {
    edit,
}

export default {
    metadata,
    settings
}