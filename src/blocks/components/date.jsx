// WordPress dependencies
const { __ } = wp.i18n

const {
    DateTimePicker,
    ToggleControl,
    TextareaControl,
} = wp.components

const {
    useState
} = wp.element

export const datePanel = ( attributes, setAttributes ) => {
    const {
        timeRestrictionLabel,
        restrictGlobal,
        restrictCustomStart,
        restrictCustomEnd,
        dateStart,
        dateEnd,
        adminOnly,
        userOnly,
    } = attributes

    let restricted = false
    if( userOnly || adminOnly || restrictGlobal || restrictCustomStart || restrictCustomEnd ) {
        restricted = true
    }
    
    return <>
        { restricted && <TextareaControl
            label={ __( 'Label when post restricted' ) }
            onChange={ ( value ) => setAttributes( { timeRestrictionLabel: value } ) }
            value={ timeRestrictionLabel }
        /> }
        <ToggleControl
            label={ __( 'Restrict to admin users' ) }
            onChange={ ( value ) => setAttributes( { adminOnly: value } ) }
            checked={ adminOnly }
        />
        { !adminOnly && <ToggleControl
                label={ __( 'Restrict to registered users' ) }
                onChange={ ( value ) => setAttributes( { userOnly: value } ) }
                checked={ userOnly }
            />
        }
        <ToggleControl
            label={ __( 'Restrict by date (site wide)' ) }
            onChange={ ( value ) => setAttributes( { restrictGlobal: value } ) }
            checked={ restrictGlobal }
        />
        { !restrictGlobal && <ToggleControl
            label={ __( 'Restrict by date (custom start)' ) }
            onChange={ ( value ) => setAttributes( { restrictCustomStart: value } ) }
            checked={ restrictCustomStart }
        /> }
        { !restrictGlobal && restrictCustomStart &&
            <DateTimePicker
                currentDate={ dateStart }
                onChange={ ( value ) => setAttributes( { dateStart: value } ) }
                is12Hour={ true }
            /> 
        }
        { !restrictGlobal && <ToggleControl
            label={ __( 'Restrict by date (custom end)' ) }
            onChange={ ( value ) => setAttributes( { restrictCustomEnd: value } ) }
            checked={ restrictCustomEnd }
        /> }
        { !restrictGlobal && restrictCustomEnd &&
            <DateTimePicker
                currentDate={ dateEnd }
                onChange={ ( value ) => setAttributes( { dateEnd: value } ) }
                is12Hour={ true }
            /> 
        }

    </>
}