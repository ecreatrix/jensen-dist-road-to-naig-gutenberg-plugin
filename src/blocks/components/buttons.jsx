// WordPress dependencies
const { __ } = wp.i18n

const {
    SelectControl,
    ToggleControl,
} = wp.components


const { apiFetch } = wp
const { addQueryArgs } = wp.url
const { useEffect, useState } = wp.element

export const buttonsControl = ( attributes, setAttributes ) => {
    const {
        uploadButton,
        uploadType,
        nfUpload,

        completeButton,
        completeType,
    } = attributes

    const [ forms, updateForms ] = useState({})

    useEffect( () => {
        apiFetch( {
            path: addQueryArgs( '/jensen/v1/ninja/forms', ),
        } ).then( ( res ) => {
           updateForms( JSON.parse( res ) )
        } )
    }, [] )
    
    return <>
        <ToggleControl
            label={ __( 'Show complete toggle' ) }
            onChange={ ( value ) => setAttributes( { completeButton: value } ) }
            checked={ completeButton }
        />
        { completeButton && <>
                <SelectControl
                    label={ __( 'Completion type', 'jensen' ) }
                    value={ completeType } 
                    onChange={ ( value ) => setAttributes( { completeType: value } ) }
                    options={ [
                        { value: '', label: 'Select' },
                        { value: 'jg_points_off-ice', label: 'Off-Ice Training' },
                        { value: 'jg_points_challenges', label: 'Challenges' },
                        { value: 'jg_points_dryland', label: 'Dryland Training' },
                        { value: 'jg_points_fitness_training', label: '6 Week Training' },
                    ] }
                />
            </>
        }
        <ToggleControl
            label={ __( 'Show upload button' ) }
            onChange={ ( value ) => setAttributes( { uploadButton: value } ) }
            checked={ uploadButton }
        />
        { uploadButton && forms && <>
                <SelectControl
                    label={ __( 'Select Upload Form', 'jensen' ) }
                    value={ nfUpload } 
                    onChange={ ( value ) => setAttributes( { nfUpload: value } ) }
                    options={ forms }
                />
                <SelectControl
                    label={ __( 'Upload type', 'jensen' ) }
                    value={ uploadType } 
                    onChange={ ( value ) => setAttributes( { uploadType: value } ) }
                    options={ [
                        { value: '', label: 'Select' },
                        { value: 'challenge-1', label: 'Challenge 1' },
                        { value: 'challenge-2', label: 'Challenge 2' },
                        { value: 'challenge-3', label: 'Challenge 3' },
                        { value: 'challenge-4', label: 'Challenge 4' },
                    ] }
                />
            </>
        }
    </>
}