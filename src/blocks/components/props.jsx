// External dependencies
import classnames from 'classnames'

// Wordpress dependencies
const {
    PanelColorSettings,
} = wp.blockEditor
const { __ } = wp.i18n

export function coloursProps(props, customClasses = [], makeHeading = false) {
    let { attributes, className, textColor, backgroundColor, align } = props

    let style = {
        color: textColor?.color,
        backgroundColor: backgroundColor?.color,
    }

    let classes = classnames( {
        [ 'wp-block-cover' ]: !! makeHeading, 
        [ `has-text-color has-${ attributes.textColor }-text-color` ]: !! attributes.textColor,
        [ `has-background-color has-${ attributes.backgroundColor }-bg-color` ]: !! attributes.backgroundColor,
        [`align${ align }`]: !! align,
        className,
        ...customClasses
    } )

    return {
        classes,
        style,
    }
}

export function colourPanel( props ) {
    const {
        backgroundColor,
        setBackgroundColor,

        textColor,
        setTextColor,
    } = props

    return <PanelColorSettings
        title={ __( 'Background Color' ) }
        initialOpen={ true }
        colorSettings={ [
            {
                value: textColor.color,
                onChange: setTextColor,
                label: __( 'Text color' ),
            },
            {
                value: backgroundColor.color,
                onChange: setBackgroundColor,
                label: __( 'Background color' ),
            },
        ] }
    >
    </PanelColorSettings>
}