// Wordpress
const {
    registerBlockType,
} = wp.blocks

// Blocks
import contentVisibility from './content-visibility'

import postCarousel from './post-carousel'
import postGrid from './post-grid'

//import memberUpload from './member-upload'
import memberComplete from './member-complete'

import training from './training'
import trainingSubblock from './training-subblock'

import pageHeading from './page-heading'

import resource from './resource'

import setUserData from './set-user-data'
import register from './register'
import profile from './profile'
import signin from './sign-in'

// Internal dependencies

// Register blocks
[ 
    contentVisibility,

    postCarousel,
    postGrid,

    training,
    trainingSubblock,
    
    //memberUpload,
    memberComplete,
    
    pageHeading,

    resource,

    signin,
    setUserData,
    register,
    profile,
].forEach( ( { metadata, settings } ) => {
    //console.log(metadata.title)
    registerBlockType( metadata, settings )
} )