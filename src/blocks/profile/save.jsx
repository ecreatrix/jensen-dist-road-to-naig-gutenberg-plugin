// External dependencies
import classnames from 'classnames'

// Internal dependencies
const { __ } = wp.i18n

const {
    InnerBlocks,
} = wp.blockEditor

// Internal dependencies

export default function save( props ) {
    const {
        attributes,
        className
    } = props

    const {
        backgroundColor,
        textColor,
        title,
    } = attributes

    const style = {
        backgroundColor: backgroundColor && backgroundColor.color,
        textColor: textColor && textColor.color,
    }

    const classes = classnames( 
        'wp-block-cover', 'profile', className, 
        backgroundColor && `has-${ backgroundColor }-background-color has-background-dim`,
        textColor && `has-${ textColor }-color has-${ textColor }-text-color`,
    )

    return (
        <div className={ classes } style={ style }><InnerBlocks.Content /></div>
    )
}
