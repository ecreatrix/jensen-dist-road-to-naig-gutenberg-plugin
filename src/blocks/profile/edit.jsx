// External dependencies

// WordPress dependencies
const { __ } = wp.i18n
const {
    withAPIData,
    Component,
    Fragment
} = wp.element
const {
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
} = wp.blockEditor

const { apiFetch } = wp

const {
    Spinner,
    SelectControl
} = wp.components

const { compose } = wp.compose
// Other Dependencies

// Internal dependencies

class Profile extends Component {
    constructor() {
        super( ...arguments )
        this.state = { 
            forms: false,
        }    
    }

    componentDidMount() {
        this.fetchRequest = apiFetch( {
            path: `/jensen/v1/ninja/forms`,
        } ).then( ( result ) => {
            if ( ! result ) {
                console.log( 'Error' )
                console.log( result )
            }

            this.setState( { forms: JSON.parse( result ) } )
        } ).catch( () => {
            this.setState( { forms: false } )
        } )    
    }

    render() {
        const {
            setAttributes,
            attributes,
            backgroundColor,
            setBackgroundColor,
            textColor,
            setTextColor,
            //className
        } = this.props

        const {
            nfProfile,
            nfPassword,
            nfPost,
            nfFamily,
            nfComment
        } = attributes

        let inspector = <InspectorControls key="inspector">
            <PanelColorSettings
                title={ __( 'Background Color' ) }
                initialOpen={ true }
                colorSettings={ [
                    {
                        value: textColor.color,
                        onChange: setTextColor,
                        label: __( 'Text color' ),
                    },
                    {
                        value: backgroundColor.color,
                        onChange: setBackgroundColor,
                        label: __( 'Background color' ),
                    },
                ] }
            >
            </PanelColorSettings>
        </InspectorControls>
   
        if( this.state.forms ) {
            //console.log(this.state.forms)

            return (<>
                { inspector }

                <div className="w-50 mx-auto text-center py-4">
                    <h3>User profile</h3>

                    <SelectControl
                        label={ __( 'Select Profile Form', 'jensen' ) }
                        value={ nfProfile } 
                        onChange={ ( value ) => setAttributes( { nfProfile: value } ) }
                        options={ this.state.forms }
                    />
                    <SelectControl
                        label={ __( 'Select Password Form', 'jensen' ) }
                        value={ nfPassword } 
                        onChange={ ( value ) => setAttributes( { nfPassword: value } ) }
                        options={ this.state.forms }
                    />
                    <SelectControl
                        label={ __( 'Select Add Activity Form', 'jensen' ) }
                        value={ nfPost } 
                        onChange={ ( value ) => setAttributes( { nfPost: value } ) }
                        options={ this.state.forms }
                    />
                    <SelectControl
                        label={ __( 'Select Add Family Form', 'jensen' ) }
                        value={ nfFamily } 
                        onChange={ ( value ) => setAttributes( { nfFamily: value } ) }
                        options={ this.state.forms }
                    />
                    <SelectControl
                        label={ __( 'Select Add Comment Form', 'jensen' ) }
                        value={ nfComment } 
                        onChange={ ( value ) => setAttributes( { nfComment: value } ) }
                        options={ this.state.forms }
                    />
                </div>
            </> )    


                /*<SelectControl
                style={ { minHeight: '10em' } }
                label={ __( 'Select Profile Form', 'jensen' ) }
                options={ this.state.forms }
                onChange={ ( value ) => setAttributes( { nfProfile: value } ) }
            />*/
        }
                /*<Disabled>
                    <ServerSideRender
                        block="jg/profile"
                        attributes={ attributes }
                    />
                </Disabled>*/
        return (
            <>
                { inspector }

                <Spinner animation="border" role="status"  variant="primary">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            </>
        )    
    }
}

export default compose(
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( Profile )