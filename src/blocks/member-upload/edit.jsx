// External dependencies

// WordPress dependencies

const {
    ServerSideRender,
    Disabled
} = wp.components

// Other Dependencies

// Internal dependencies

function MemberUpload( props ) {
    return (
        <>
            <Disabled>
                <ServerSideRender
                    block="jg/member-upload"
                />
            </Disabled>
        </>
    )
}

export default MemberUpload 