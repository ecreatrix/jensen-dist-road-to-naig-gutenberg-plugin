// External dependencies

// WordPress dependencies

// Internal dependencies
import metadata from './block.json'
import edit from './edit'

const settings = {
    edit,
}

export default {
    metadata,
    settings
}