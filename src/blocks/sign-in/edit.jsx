// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const {
    Component,
    Fragment
} = wp.element

const {
    MediaUpload, 
    MediaUploadCheck,
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
} = wp.blockEditor

const {
    ServerSideRender,
    PanelBody, 
    TextControl,
    Disabled,
    Button
} = wp.components

const { compose } = wp.compose

const {
    withSelect,
    select,
} = wp.data
// Other Dependencies

// Internal dependencies

export default function Signin( { 
    attributes,
    setAttributes,
} ) {
    return (
        <>
            <Disabled>
                <ServerSideRender
                    block="jg/signin"
                    attributes={ attributes }
                />
            </Disabled>
        </>
    )
}
