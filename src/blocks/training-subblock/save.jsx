// External dependencies
import classnames from 'classnames/dedupe'

// Internal dependencies
const { __ } = wp.i18n

const {
    InnerBlocks,
} = wp.blockEditor

// Internal dependencies

export default function save( { className, attributes: { title } } ) {
    return <div className={ className }>
        <div className="label">{ title }</div>
        <div className="content"><InnerBlocks.Content /></div>
    </div>
}