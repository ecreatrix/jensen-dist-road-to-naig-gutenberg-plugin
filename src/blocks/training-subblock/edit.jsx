// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const {
    InnerBlocks,
    useBlockProps,
    __experimentalUseInnerBlocksProps: useInnerBlocksProps,
} = wp.blockEditor

const {
    TextControl,
} = wp.components

// Other Dependencies

// Internal dependencies
const ALLOWED_BLOCKS = [ 'core/column', 'core/paragraph', 'core/heading', 'core/list', 'core/group', 'core/separator', 'core/spacer', 'core/buttons', 'core/image', 'core/video' ];

function TrainingSubblock( { 
        attributes: { title },
        setAttributes,
        className,
} ) {
    const blockProps = useBlockProps( {
        className
    } ) 

    const innerBlocksProps = useInnerBlocksProps( blockProps, {
        allowedBlocks: ALLOWED_BLOCKS,
        template: [
            [
                'core/paragraph',
                {
                    placeholder: __( 'Add Content...' ),
                },
            ],
        ]
    } )

    return (
        <>
            <div { ...blockProps }>
                <div className="wp-block-cover__inner-container">
                    <div>
                        <TextControl
                            label={ __( 'Section Title' ) }
                            onChange={ ( value ) => setAttributes( { title: value } ) }
                            value={ title }
                        />
                    </div>
                    
                    <div { ...innerBlocksProps } />
                </div>
            </div>
        </>
    )
}

export default TrainingSubblock 