// External dependencies

// WordPress dependencies


// Internal dependencies
import metadata from './block'
import edit from './edit'
import save from './save'

const settings = {
    edit,
    save,
}

export default {
    metadata,
    settings
}