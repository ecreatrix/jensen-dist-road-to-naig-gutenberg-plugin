// External dependencies
import classnames from 'classnames/dedupe'

// Internal dependencies
const { __ } = wp.i18n

const {
    InnerBlocks,
} = wp.blockEditor

// Internal dependencies

export default function save( props ) {
    const {
        attributes,
        className
    } = props

    const {
        title,
        blockId,
        text,
        link
    } = attributes;

    let resourceSublabel
    if( text ) {
        resourceSublabel = <div class="resource-sublabel">{ text }</div>
    }

    let introDiv = <div className="label collapsed" data-bs-toggle="collapse" href={ `#${ blockId }` } role="button" aria-expanded="false" aria-controls={ blockId }>
            <span>{ title }</span>
            <button className="btn btn-primary">Watch Video</button>
            { resourceSublabel }
        </div>
    let childClasses = 'video'

    if( link ) {
        introDiv = <div className="label">
            <span>{ title }</span>
            <a className="btn btn-primary" href={`${ link }` } target="_blank">Go to Link</a>
            { resourceSublabel }
        </div>
        childClasses = 'link'
    }

    return <div className={ classnames( [ childClasses, className ] ) }>
        { introDiv }
        <div id={ blockId } className="resource-text collapse"><InnerBlocks.Content /></div>
    </div>
}
