// External dependencies
import classnames from 'classnames';

// WordPress dependencies
const { __ } = wp.i18n

const {
    InnerBlocks,
    InspectorControls, 
    useBlockProps,
    URLPopover,
    URLInput,
    __experimentalUseInnerBlocksProps,
} = wp.blockEditor

const {
    Button,
    PanelBody, 
    TextControl,
    TextareaControl,
} = wp.components

const { select, useSelect } = wp.data;

// Other Dependencies

// Internal dependencies
export default function Resource( props ) {
    const {
        attributes,
        setAttributes,
        className,
        clientId
    } = props

    const {
        title,
        blockId,
        childClasses,
        text, 
        link
    } = attributes

    const blockProps = useBlockProps( {
        className
    } ) 

    //if ( ! blockId ) {
        setAttributes( { blockId: `resource-${ clientId.split('-')[0] }` } );
    //}

    const { blockCount } = useSelect( select => ({
        blockCount: select('core/block-editor').getBlockCount( clientId )
    }))

    const innerBlocksProps = __experimentalUseInnerBlocksProps( blockProps, {
        allowedBlocks: [ 'core/embed', 'core/video' ],
    } );

    return <>
        <TextControl
            label={ __( 'Resource(s) Label' ) }
            onChange={ ( value ) => setAttributes( { title: value } ) }
            value={ title }
        />
        <TextareaControl
            label={ __( 'Resource(s) Description' ) }
            onChange={ ( value ) => setAttributes( { text: value } ) }
            value={ text }
        />
        { blockCount === 0 && <URLInput
            label={ __( 'External Link (instead of embed): ' ) }
            value={ link }
            onChange={ ( nextURL ) => setAttributes( { link: nextURL } ) }
            placeholder={ '' }
            disableSuggestions={ true }
        /> }
        { link === '' || !link && <div { ...innerBlocksProps } /> }
    </>
}