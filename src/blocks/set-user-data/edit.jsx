// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const {
    Component,
    Fragment
} = wp.element

const {
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
} = wp.blockEditor

const { apiFetch } = wp

const {
    ServerSideRender,
    Disabled,
    Spinner,
    SelectControl
} = wp.components

const { compose } = wp.compose

// Other Dependencies

// Internal dependencies

class FinishRegistration extends Component {
    constructor() {
        super( ...arguments )
        this.state = { 
            forms: false,
        }    
    }

    componentDidMount() {
        this.fetchRequest = apiFetch( {
            path: `/jensen/v1/ninja/forms`,
        } ).then( ( result ) => {
            if ( ! result ) {
                console.log( 'Error' )
                console.log( result )
            }

            this.setState( { forms: JSON.parse( result ) } )
        } ).catch( () => {
            this.setState( { forms: false } )
        } )    
    }

    render() {
        const {
            setAttributes,
            attributes,
            backgroundColor,
            setBackgroundColor,
            textColor,
            setTextColor,
            //className
        } = this.props

        const {
            nfRegistration
        } = attributes

        let inspector = <InspectorControls key="inspector">
            <PanelColorSettings
                title={ __( 'Background Color' ) }
                initialOpen={ true }
                colorSettings={ [
                    {
                        value: textColor.color,
                        onChange: setTextColor,
                        label: __( 'Text color' ),
                    },
                    {
                        value: backgroundColor.color,
                        onChange: setBackgroundColor,
                        label: __( 'Background color' ),
                    },
                ] }
            >
            </PanelColorSettings>
        </InspectorControls>
   
        if( this.state.forms ) {
            //console.log(this.state.forms)

            return (<>
                { inspector }

                <div className="w-50 mx-auto text-center py-4">
                    <h3>User profile</h3>

                    <SelectControl
                        label={ __( 'Select Registration Form', 'jensen' ) }
                        value={ nfRegistration } 
                        onChange={ ( value ) => setAttributes( { nfRegistration: value } ) }
                        options={ this.state.forms }
                    />
                </div>

                <Disabled>
                    <ServerSideRender
                        block="jg/set-user-data"
                        attributes={ attributes }
                    />
                </Disabled>
            </> )    
        }
        
        return (
            <>
                { inspector }

                <Spinner animation="border" role="status"  variant="primary">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            </>
        )    

        return (
            <>
                <InspectorControls key="inspector">
                    <PanelColorSettings
                        title={ __( 'Background Color' ) }
                        initialOpen={ true }
                        colorSettings={ [
                            {
                                value: textColor.color,
                                onChange: setTextColor,
                                label: __( 'Text color' ),
                            },
                            {
                                value: backgroundColor.color,
                                onChange: setBackgroundColor,
                                label: __( 'Background color' ),
                            },
                        ] }
                    >
                    </PanelColorSettings>
                    <PanelBody
                        title={ __( 'Ninja Form' ) }
                        initialOpen={ false }
                    >
                        <TextControl
                            label={ __( 'Registration' ) }
                            value={ nfRegistration }
                            onChange={ ( value ) =>
                                setAttributes( { nfRegistration: value } )
                            }
                        />
                    </PanelBody>
                </InspectorControls>
            </>
        )
    }
}

export default compose(
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( FinishRegistration )
