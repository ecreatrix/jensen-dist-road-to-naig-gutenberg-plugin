// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    Component,
    Fragment
} = wp.element

const {
    InnerBlocks,
    InspectorControls,
    withColors,
} = wp.blockEditor

const {
    PanelBody, 
} = wp.components

const { useEffect, useState } = wp.element

// Other Dependencies

// Internal dependencies
import { coloursProps, colourPanel } from '../components/props'
import { datePanel } from '../components/date'

function Visibility( props ) {
    const {
        attributes,
        setAttributes,

        backgroundColor,
        textColor,

        className,
    } = props

    const allColoursProps = coloursProps(props, [], true)

    let inspectorControls = <>
        <InspectorControls key="inspector">
            { colourPanel(props) }

            <PanelBody title={ __( 'Visibility settings' ) }>
                { datePanel( props ) }
            </PanelBody>
        </InspectorControls>
    </>

    return (
        <>
            { inspectorControls }
            
            <div 
                className={ allColoursProps.classes }
                style={ { ...allColoursProps.style } }
            >
                <div className="wp-block-cover__inner-container">
                    <InnerBlocks
                        template={ [
                            [
                                'core/paragraph',
                                {
                                    placeholder: __( 'Add Content...' ),
                                },
                            ],
                        ] }
                    />
                </div>
            </div>
        </>
    )
}

export default compose(
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( Visibility );