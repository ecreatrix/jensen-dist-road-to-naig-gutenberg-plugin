"use strict";
(self["webpackChunkjg_road_to_naig"] = self["webpackChunkjg_road_to_naig"] || []).push([["/assets/scripts/blocks"],{

/***/ "./roadtonaig-gutenberg/src/blocks/blocks.jsx":
/*!****************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/blocks.jsx ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _content_visibility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./content-visibility */ "./roadtonaig-gutenberg/src/blocks/content-visibility/index.jsx");
/* harmony import */ var _post_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./post-carousel */ "./roadtonaig-gutenberg/src/blocks/post-carousel/index.jsx");
/* harmony import */ var _post_grid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./post-grid */ "./roadtonaig-gutenberg/src/blocks/post-grid/index.jsx");
/* harmony import */ var _member_complete__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./member-complete */ "./roadtonaig-gutenberg/src/blocks/member-complete/index.jsx");
/* harmony import */ var _training__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./training */ "./roadtonaig-gutenberg/src/blocks/training/index.jsx");
/* harmony import */ var _training_subblock__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./training-subblock */ "./roadtonaig-gutenberg/src/blocks/training-subblock/index.jsx");
/* harmony import */ var _page_heading__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-heading */ "./roadtonaig-gutenberg/src/blocks/page-heading/index.jsx");
/* harmony import */ var _resource__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./resource */ "./roadtonaig-gutenberg/src/blocks/resource/index.jsx");
/* harmony import */ var _set_user_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./set-user-data */ "./roadtonaig-gutenberg/src/blocks/set-user-data/index.jsx");
/* harmony import */ var _register__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./register */ "./roadtonaig-gutenberg/src/blocks/register/index.jsx");
/* harmony import */ var _profile__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./profile */ "./roadtonaig-gutenberg/src/blocks/profile/index.jsx");
/* harmony import */ var _sign_in__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sign-in */ "./roadtonaig-gutenberg/src/blocks/sign-in/index.jsx");
// Wordpress
var registerBlockType = wp.blocks.registerBlockType; // Blocks



 //import memberUpload from './member-upload'









 // Internal dependencies
// Register blocks

[_content_visibility__WEBPACK_IMPORTED_MODULE_0__["default"], _post_carousel__WEBPACK_IMPORTED_MODULE_1__["default"], _post_grid__WEBPACK_IMPORTED_MODULE_2__["default"], _training__WEBPACK_IMPORTED_MODULE_4__["default"], _training_subblock__WEBPACK_IMPORTED_MODULE_5__["default"], //memberUpload,
_member_complete__WEBPACK_IMPORTED_MODULE_3__["default"], _page_heading__WEBPACK_IMPORTED_MODULE_6__["default"], _resource__WEBPACK_IMPORTED_MODULE_7__["default"], _sign_in__WEBPACK_IMPORTED_MODULE_11__["default"], _set_user_data__WEBPACK_IMPORTED_MODULE_8__["default"], _register__WEBPACK_IMPORTED_MODULE_9__["default"], _profile__WEBPACK_IMPORTED_MODULE_10__["default"]].forEach(function (_ref) {
  var metadata = _ref.metadata,
      settings = _ref.settings;
  //console.log(metadata.title)
  registerBlockType(metadata, settings);
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/components/buttons.jsx":
/*!****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/components/buttons.jsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "buttonsControl": function() { return /* binding */ buttonsControl; }
/* harmony export */ });


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// WordPress dependencies
var __ = wp.i18n.__;
var _wp$components = wp.components,
    SelectControl = _wp$components.SelectControl,
    ToggleControl = _wp$components.ToggleControl;
var _wp = wp,
    apiFetch = _wp.apiFetch;
var addQueryArgs = wp.url.addQueryArgs;
var _wp$element = wp.element,
    useEffect = _wp$element.useEffect,
    useState = _wp$element.useState;
var buttonsControl = function buttonsControl(attributes, setAttributes) {
  var uploadButton = attributes.uploadButton,
      uploadType = attributes.uploadType,
      nfUpload = attributes.nfUpload,
      completeButton = attributes.completeButton,
      completeType = attributes.completeType;

  var _useState = useState({}),
      _useState2 = _slicedToArray(_useState, 2),
      forms = _useState2[0],
      updateForms = _useState2[1];

  useEffect(function () {
    apiFetch({
      path: addQueryArgs('/jensen/v1/ninja/forms')
    }).then(function (res) {
      updateForms(JSON.parse(res));
    });
  }, []);
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(ToggleControl, {
    label: __('Show complete toggle'),
    onChange: function onChange(value) {
      return setAttributes({
        completeButton: value
      });
    },
    checked: completeButton
  }), completeButton && wp.element.createElement(React.Fragment, null, wp.element.createElement(SelectControl, {
    label: __('Completion type', 'jensen'),
    value: completeType,
    onChange: function onChange(value) {
      return setAttributes({
        completeType: value
      });
    },
    options: [{
      value: '',
      label: 'Select'
    }, {
      value: 'jg_points_off-ice',
      label: 'Off-Ice Training'
    }, {
      value: 'jg_points_challenges',
      label: 'Challenges'
    }, {
      value: 'jg_points_dryland',
      label: 'Dryland Training'
    }, {
      value: 'jg_points_fitness_training',
      label: '6 Week Training'
    }]
  })), wp.element.createElement(ToggleControl, {
    label: __('Show upload button'),
    onChange: function onChange(value) {
      return setAttributes({
        uploadButton: value
      });
    },
    checked: uploadButton
  }), uploadButton && forms && wp.element.createElement(React.Fragment, null, wp.element.createElement(SelectControl, {
    label: __('Select Upload Form', 'jensen'),
    value: nfUpload,
    onChange: function onChange(value) {
      return setAttributes({
        nfUpload: value
      });
    },
    options: forms
  }), wp.element.createElement(SelectControl, {
    label: __('Upload type', 'jensen'),
    value: uploadType,
    onChange: function onChange(value) {
      return setAttributes({
        uploadType: value
      });
    },
    options: [{
      value: '',
      label: 'Select'
    }, {
      value: 'challenge-1',
      label: 'Challenge 1'
    }, {
      value: 'challenge-2',
      label: 'Challenge 2'
    }, {
      value: 'challenge-3',
      label: 'Challenge 3'
    }, {
      value: 'challenge-4',
      label: 'Challenge 4'
    }]
  })));
};

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/components/date.jsx":
/*!*************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/components/date.jsx ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "datePanel": function() { return /* binding */ datePanel; }
/* harmony export */ });

// WordPress dependencies
var __ = wp.i18n.__;
var _wp$components = wp.components,
    DateTimePicker = _wp$components.DateTimePicker,
    ToggleControl = _wp$components.ToggleControl,
    TextareaControl = _wp$components.TextareaControl;
var useState = wp.element.useState;
var datePanel = function datePanel(attributes, setAttributes) {
  var timeRestrictionLabel = attributes.timeRestrictionLabel,
      restrictGlobal = attributes.restrictGlobal,
      restrictCustomStart = attributes.restrictCustomStart,
      restrictCustomEnd = attributes.restrictCustomEnd,
      dateStart = attributes.dateStart,
      dateEnd = attributes.dateEnd,
      adminOnly = attributes.adminOnly,
      userOnly = attributes.userOnly;
  var restricted = false;

  if (userOnly || adminOnly || restrictGlobal || restrictCustomStart || restrictCustomEnd) {
    restricted = true;
  }

  return wp.element.createElement(React.Fragment, null, restricted && wp.element.createElement(TextareaControl, {
    label: __('Label when post restricted'),
    onChange: function onChange(value) {
      return setAttributes({
        timeRestrictionLabel: value
      });
    },
    value: timeRestrictionLabel
  }), wp.element.createElement(ToggleControl, {
    label: __('Restrict to admin users'),
    onChange: function onChange(value) {
      return setAttributes({
        adminOnly: value
      });
    },
    checked: adminOnly
  }), !adminOnly && wp.element.createElement(ToggleControl, {
    label: __('Restrict to registered users'),
    onChange: function onChange(value) {
      return setAttributes({
        userOnly: value
      });
    },
    checked: userOnly
  }), wp.element.createElement(ToggleControl, {
    label: __('Restrict by date (site wide)'),
    onChange: function onChange(value) {
      return setAttributes({
        restrictGlobal: value
      });
    },
    checked: restrictGlobal
  }), !restrictGlobal && wp.element.createElement(ToggleControl, {
    label: __('Restrict by date (custom start)'),
    onChange: function onChange(value) {
      return setAttributes({
        restrictCustomStart: value
      });
    },
    checked: restrictCustomStart
  }), !restrictGlobal && restrictCustomStart && wp.element.createElement(DateTimePicker, {
    currentDate: dateStart,
    onChange: function onChange(value) {
      return setAttributes({
        dateStart: value
      });
    },
    is12Hour: true
  }), !restrictGlobal && wp.element.createElement(ToggleControl, {
    label: __('Restrict by date (custom end)'),
    onChange: function onChange(value) {
      return setAttributes({
        restrictCustomEnd: value
      });
    },
    checked: restrictCustomEnd
  }), !restrictGlobal && restrictCustomEnd && wp.element.createElement(DateTimePicker, {
    currentDate: dateEnd,
    onChange: function onChange(value) {
      return setAttributes({
        dateEnd: value
      });
    },
    is12Hour: true
  }));
};

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/components/props.jsx":
/*!**************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/components/props.jsx ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "coloursProps": function() { return /* binding */ coloursProps; },
/* harmony export */   "colourPanel": function() { return /* binding */ colourPanel; }
/* harmony export */ });
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // Wordpress dependencies


var PanelColorSettings = wp.blockEditor.PanelColorSettings;
var __ = wp.i18n.__;
function coloursProps(props) {
  var _objectSpread2;

  var customClasses = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var makeHeading = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var attributes = props.attributes,
      className = props.className,
      textColor = props.textColor,
      backgroundColor = props.backgroundColor,
      align = props.align;
  var style = {
    color: textColor === null || textColor === void 0 ? void 0 : textColor.color,
    backgroundColor: backgroundColor === null || backgroundColor === void 0 ? void 0 : backgroundColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()(_objectSpread((_objectSpread2 = {}, _defineProperty(_objectSpread2, 'wp-block-cover', !!makeHeading), _defineProperty(_objectSpread2, "has-text-color has-".concat(attributes.textColor, "-text-color"), !!attributes.textColor), _defineProperty(_objectSpread2, "has-background-color has-".concat(attributes.backgroundColor, "-bg-color"), !!attributes.backgroundColor), _defineProperty(_objectSpread2, "align".concat(align), !!align), _defineProperty(_objectSpread2, "className", className), _objectSpread2), customClasses));
  return {
    classes: classes,
    style: style
  };
}
function colourPanel(props) {
  var backgroundColor = props.backgroundColor,
      setBackgroundColor = props.setBackgroundColor,
      textColor = props.textColor,
      setTextColor = props.setTextColor;
  return wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  });
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/content-visibility/edit.jsx":
/*!*********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/content-visibility/edit.jsx ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_props__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/props */ "./roadtonaig-gutenberg/src/blocks/components/props.jsx");
/* harmony import */ var _components_date__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/date */ "./roadtonaig-gutenberg/src/blocks/components/date.jsx");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors;
var PanelBody = wp.components.PanelBody;
var _wp$element2 = wp.element,
    useEffect = _wp$element2.useEffect,
    useState = _wp$element2.useState; // Other Dependencies
// Internal dependencies





function Visibility(props) {
  var attributes = props.attributes,
      setAttributes = props.setAttributes,
      backgroundColor = props.backgroundColor,
      textColor = props.textColor,
      className = props.className;
  var allColoursProps = (0,_components_props__WEBPACK_IMPORTED_MODULE_0__.coloursProps)(props, [], true);
  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, (0,_components_props__WEBPACK_IMPORTED_MODULE_0__.colourPanel)(props), wp.element.createElement(PanelBody, {
    title: __('Visibility settings')
  }, (0,_components_date__WEBPACK_IMPORTED_MODULE_1__.datePanel)(props))));
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", {
    className: allColoursProps.classes,
    style: _objectSpread({}, allColoursProps.style)
  }, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement(InnerBlocks, {
    template: [['core/paragraph', {
      placeholder: __('Add Content...')
    }]]
  }))));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(Visibility));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/content-visibility/index.jsx":
/*!**********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/content-visibility/index.jsx ***!
  \**********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/content-visibility/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/content-visibility/edit.jsx");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/content-visibility/save.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies



var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/content-visibility/save.jsx":
/*!*********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/content-visibility/save.jsx ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_props__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/props */ "./roadtonaig-gutenberg/src/blocks/components/props.jsx");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // Internal dependencies

var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies



function save(props) {
  var allColoursProps = (0,_components_props__WEBPACK_IMPORTED_MODULE_1__.coloursProps)(props, [], true);
  return wp.element.createElement("div", {
    className: allColoursProps.classes,
    style: _objectSpread({}, allColoursProps.style)
  }, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement(InnerBlocks.Content, null)));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/member-complete/edit.jsx":
/*!******************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/member-complete/edit.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);

// External dependencies
// WordPress dependencies
var _wp$components = wp.components,
    ServerSideRender = _wp$components.ServerSideRender,
    Disabled = _wp$components.Disabled; // Other Dependencies
// Internal dependencies

function MemberComplete(props) {
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(Disabled, null, "Complete button for user"));
}

/* harmony default export */ __webpack_exports__["default"] = (MemberComplete);

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/member-complete/index.jsx":
/*!*******************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/member-complete/index.jsx ***!
  \*******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/member-complete/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/member-complete/edit.jsx");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/member-complete/save.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies



var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/member-complete/save.jsx":
/*!******************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/member-complete/save.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_props__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/props */ "./roadtonaig-gutenberg/src/blocks/components/props.jsx");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // Internal dependencies

var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies



function save(props) {
  var allColoursProps = (0,_components_props__WEBPACK_IMPORTED_MODULE_1__.coloursProps)(props, [], true);
  return wp.element.createElement("div", {
    className: allColoursProps.classes,
    style: _objectSpread({}, allColoursProps.style)
  }, "Complete Button");
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/page-heading/edit.jsx":
/*!***************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/page-heading/edit.jsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // WordPress dependencies

var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner;


var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select; // Other Dependencies
// Internal dependencies

function PageHeading(_ref) {
  var _classnames;

  var isRequesting = _ref.isRequesting,
      setAttributes = _ref.setAttributes,
      title = _ref.attributes.title,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className,
      pageTitle = _ref.pageTitle;
  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  })));

  if (isRequesting) {
    return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__["default"],
      label: __('Activity Heading')
    }, wp.element.createElement(Spinner, null)));
  }

  if (pageTitle !== title) {
    setAttributes({
      title: pageTitle
    });
  }

  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('wp-block-cover', 'page-heading', 'has-background-dim', className, (_classnames = {}, _defineProperty(_classnames, backgroundColor["class"], backgroundColor["class"]), _defineProperty(_classnames, textColor["class"], textColor["class"]), _classnames));
  var blockProps = useBlockProps({
    className: classes,
    style: {
      backgroundColor: backgroundColor && backgroundColor.color
    }
  });
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", blockProps, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement("h1", {
    className: "entry-title"
  }, title))));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var pageTitle = select("core/editor").getEditedPostAttribute('title');
  return {
    pageTitle: pageTitle
  };
}), withColors('backgroundColor', 'textColor'))(PageHeading));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/page-heading/index.jsx":
/*!****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/page-heading/index.jsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/page-heading/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/page-heading/edit.jsx");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/page-heading/save.jsx");
// External dependencies
// WordPress dependencies
var __ = wp.i18n.__; // Internal dependencies





var settings = {
  icon: wp.element.createElement("svg", {
    viewBox: "0 0 16 10",
    xmlns: "http://www.w3.org/2000/svg"
  }, wp.element.createElement("rect", {
    fill: "#000000",
    x: "0",
    y: "0.55",
    width: "16",
    height: "8.7"
  }), wp.element.createElement("path", {
    d: "M10.029801,2.47339585 L7.46584183,2.47339585 L7.46584183,2.44287255 L7.19113192,2.50391915 C6.4890955,2.62601245 5.97019902,3.23647885 5.97019902,3.96903865 C5.97019902,4.70159835 6.4890955,5.31206485 7.19113192,5.43415815 L7.40479518,5.46468145 L7.40479518,7.35712745 L7.86264502,7.35712745 L7.86264502,2.93124565 L8.74782137,2.93124565 L8.74782137,7.35712745 L9.20567121,7.35712745 L9.20567121,2.93124565 L10.029801,2.93124565 L10.029801,2.47339585 Z",
    fill: "#FFFFFF"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/page-heading/save.jsx":
/*!***************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/page-heading/save.jsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // Wordpress dependencies


var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies

function save(props) {
  var attributes = props.attributes,
      className = props.className;
  var backgroundColor = attributes.backgroundColor,
      textColor = attributes.textColor,
      title = attributes.title;
  var style = {
    backgroundColor: backgroundColor && backgroundColor.color,
    textColor: textColor && textColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('wp-block-cover', 'page-heading', className, backgroundColor && "has-".concat(backgroundColor, "-background-color has-background-dim"), textColor && "has-".concat(textColor, "-color has-").concat(textColor, "-text-color"));
  return wp.element.createElement("div", {
    className: classes,
    style: style
  }, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement("h1", {
    className: "entry-title"
  }, title)));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/post-carousel/edit.jsx":
/*!****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/post-carousel/edit.jsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl,
    ServerSideRender = _wp$components.ServerSideRender,
    Disabled = _wp$components.Disabled;
var useSelect = wp.data.useSelect;
var coreStore = wp.coreData.store; // Other Dependencies
// Internal dependencies

function PostCarousel(_ref) {
  var setAttributes = _ref.setAttributes,
      attributes = _ref.attributes,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className;
  var numSlides = attributes.numSlides,
      postType = attributes.postType;

  var _useSelect = useSelect(function (select) {
    var _select = select(coreStore),
        getEntityRecords = _select.getEntityRecords,
        isResolving = _select.isResolving,
        hasFinishedResolution = _select.hasFinishedResolution,
        getPostTypes = _select.getPostTypes;

    var postsParameters = ['postType', postType, {
      per_page: numSlides
    }];
    var hasResolvedPosts = hasFinishedResolution('getEntityRecords', postsParameters);
    var posts = getEntityRecords.apply(void 0, postsParameters) || null;
    var selectTypes = [{
      value: false,
      label: 'Select Type'
    }];
    var allTypes = getPostTypes();
    var hasResolvedTypes = hasFinishedResolution('getPostTypes');
    hasResolvedTypes && allTypes.map(function (type, i) {
      if (type.slug !== 'attachment' && type.slug !== 'wp_block' && type.slug !== 'wp_template') {
        selectTypes = [].concat(_toConsumableArray(selectTypes), [{
          value: type.slug,
          label: type.labels.name
        }]);
      }
    });
    return {
      selectTypes: selectTypes,
      posts: posts
    };
  }, [numSlides, postType]),
      posts = _useSelect.posts,
      selectTypes = _useSelect.selectTypes;

  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  }), wp.element.createElement(PanelBody, {
    title: __('Content')
  }, wp.element.createElement(SelectControl, {
    label: __('Select Post Type'),
    value: postType,
    options: selectTypes,
    onChange: function onChange(value) {
      return setAttributes({
        postType: value
      });
    }
  }))));
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Disabled, null, wp.element.createElement(ServerSideRender, {
    block: "jg/post-carousel",
    attributes: attributes
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(PostCarousel));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/post-carousel/index.jsx":
/*!*****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/post-carousel/index.jsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/post-carousel/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/post-carousel/edit.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies


var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/post-grid/edit.jsx":
/*!************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/post-grid/edit.jsx ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl,
    ServerSideRender = _wp$components.ServerSideRender,
    Disabled = _wp$components.Disabled;
var useSelect = wp.data.useSelect;
var coreStore = wp.coreData.store; // Other Dependencies
// Internal dependencies

function PostGrid(_ref) {
  var setAttributes = _ref.setAttributes,
      attributes = _ref.attributes,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className;
  var postType = attributes.postType;

  var _useSelect = useSelect(function (select) {
    var _select = select(coreStore),
        getEntityRecords = _select.getEntityRecords,
        isResolving = _select.isResolving,
        hasFinishedResolution = _select.hasFinishedResolution,
        getPostTypes = _select.getPostTypes;

    var postsParameters = ['postType', postType];
    var hasResolvedPosts = hasFinishedResolution('getEntityRecords', postsParameters);
    var posts = getEntityRecords.apply(void 0, postsParameters) || null;
    var selectTypes = [{
      value: false,
      label: 'Select Type'
    }];
    var allTypes = getPostTypes();
    var hasResolvedTypes = hasFinishedResolution('getPostTypes');
    hasResolvedTypes && allTypes.map(function (type, i) {
      if (type.slug !== 'attachment' && type.slug !== 'wp_block' && type.slug !== 'wp_template') {
        selectTypes = [].concat(_toConsumableArray(selectTypes), [{
          value: type.slug,
          label: type.labels.name
        }]);
      }
    });
    return {
      selectTypes: selectTypes,
      posts: posts
    };
  }, [postType]),
      posts = _useSelect.posts,
      selectTypes = _useSelect.selectTypes;

  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  }), wp.element.createElement(PanelBody, {
    title: __('Content')
  }, wp.element.createElement(SelectControl, {
    label: __('Select Post Type'),
    value: postType,
    options: selectTypes,
    onChange: function onChange(value) {
      return setAttributes({
        postType: value
      });
    }
  }))));
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Disabled, null, wp.element.createElement(ServerSideRender, {
    block: "jg/post-grid",
    attributes: attributes
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(PostGrid));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/post-grid/index.jsx":
/*!*************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/post-grid/index.jsx ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/post-grid/edit.jsx");
/* harmony import */ var _block__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./block */ "./roadtonaig-gutenberg/src/blocks/post-grid/block.json");
// External dependencies
// WordPress dependencies
// Internal dependencies


var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_0__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block__WEBPACK_IMPORTED_MODULE_1__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/profile/edit.jsx":
/*!**********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/profile/edit.jsx ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }



function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var _wp$element = wp.element,
    withAPIData = _wp$element.withAPIData,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings;
var _wp = wp,
    apiFetch = _wp.apiFetch;
var _wp$components = wp.components,
    Spinner = _wp$components.Spinner,
    SelectControl = _wp$components.SelectControl;
var compose = wp.compose.compose; // Other Dependencies
// Internal dependencies

var Profile = /*#__PURE__*/function (_Component) {
  _inherits(Profile, _Component);

  var _super = _createSuper(Profile);

  function Profile() {
    var _this;

    _classCallCheck(this, Profile);

    _this = _super.apply(this, arguments);
    _this.state = {
      forms: false
    };
    return _this;
  }

  _createClass(Profile, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.fetchRequest = apiFetch({
        path: "/jensen/v1/ninja/forms"
      }).then(function (result) {
        if (!result) {
          console.log('Error');
          console.log(result);
        }

        _this2.setState({
          forms: JSON.parse(result)
        });
      })["catch"](function () {
        _this2.setState({
          forms: false
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          setAttributes = _this$props.setAttributes,
          attributes = _this$props.attributes,
          backgroundColor = _this$props.backgroundColor,
          setBackgroundColor = _this$props.setBackgroundColor,
          textColor = _this$props.textColor,
          setTextColor = _this$props.setTextColor;
      var nfProfile = attributes.nfProfile,
          nfPassword = attributes.nfPassword,
          nfPost = attributes.nfPost,
          nfFamily = attributes.nfFamily,
          nfComment = attributes.nfComment;
      var inspector = wp.element.createElement(InspectorControls, {
        key: "inspector"
      }, wp.element.createElement(PanelColorSettings, {
        title: __('Background Color'),
        initialOpen: true,
        colorSettings: [{
          value: textColor.color,
          onChange: setTextColor,
          label: __('Text color')
        }, {
          value: backgroundColor.color,
          onChange: setBackgroundColor,
          label: __('Background color')
        }]
      }));

      if (this.state.forms) {
        //console.log(this.state.forms)
        return wp.element.createElement(React.Fragment, null, inspector, wp.element.createElement("div", {
          className: "w-50 mx-auto text-center py-4"
        }, wp.element.createElement("h3", null, "User profile"), wp.element.createElement(SelectControl, {
          label: __('Select Profile Form', 'jensen'),
          value: nfProfile,
          onChange: function onChange(value) {
            return setAttributes({
              nfProfile: value
            });
          },
          options: this.state.forms
        }), wp.element.createElement(SelectControl, {
          label: __('Select Password Form', 'jensen'),
          value: nfPassword,
          onChange: function onChange(value) {
            return setAttributes({
              nfPassword: value
            });
          },
          options: this.state.forms
        }), wp.element.createElement(SelectControl, {
          label: __('Select Add Activity Form', 'jensen'),
          value: nfPost,
          onChange: function onChange(value) {
            return setAttributes({
              nfPost: value
            });
          },
          options: this.state.forms
        }), wp.element.createElement(SelectControl, {
          label: __('Select Add Family Form', 'jensen'),
          value: nfFamily,
          onChange: function onChange(value) {
            return setAttributes({
              nfFamily: value
            });
          },
          options: this.state.forms
        }), wp.element.createElement(SelectControl, {
          label: __('Select Add Comment Form', 'jensen'),
          value: nfComment,
          onChange: function onChange(value) {
            return setAttributes({
              nfComment: value
            });
          },
          options: this.state.forms
        })));
        /*<SelectControl
        style={ { minHeight: '10em' } }
        label={ __( 'Select Profile Form', 'jensen' ) }
        options={ this.state.forms }
        onChange={ ( value ) => setAttributes( { nfProfile: value } ) }
        />*/
      }
      /*<Disabled>
          <ServerSideRender
              block="jg/profile"
              attributes={ attributes }
          />
      </Disabled>*/


      return wp.element.createElement(React.Fragment, null, inspector, wp.element.createElement(Spinner, {
        animation: "border",
        role: "status",
        variant: "primary"
      }, wp.element.createElement("span", {
        className: "sr-only"
      }, "Loading...")));
    }
  }]);

  return Profile;
}(Component);

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(Profile));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/profile/index.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/profile/index.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/profile/block.json");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/profile/save.jsx");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/profile/edit.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies




var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_2__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/profile/save.jsx":
/*!**********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/profile/save.jsx ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // Internal dependencies


var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies

function save(props) {
  var attributes = props.attributes,
      className = props.className;
  var backgroundColor = attributes.backgroundColor,
      textColor = attributes.textColor,
      title = attributes.title;
  var style = {
    backgroundColor: backgroundColor && backgroundColor.color,
    textColor: textColor && textColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('wp-block-cover', 'profile', className, backgroundColor && "has-".concat(backgroundColor, "-background-color has-background-dim"), textColor && "has-".concat(textColor, "-color has-").concat(textColor, "-text-color"));
  return wp.element.createElement("div", {
    className: classes,
    style: style
  }, wp.element.createElement(InnerBlocks.Content, null));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/register/edit.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/register/edit.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }



function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings;
var _wp = wp,
    apiFetch = _wp.apiFetch;
var _wp$components = wp.components,
    Spinner = _wp$components.Spinner,
    SelectControl = _wp$components.SelectControl;
var compose = wp.compose.compose; // Other Dependencies
// Internal dependencies

var FinishRegistration = /*#__PURE__*/function (_Component) {
  _inherits(FinishRegistration, _Component);

  var _super = _createSuper(FinishRegistration);

  function FinishRegistration() {
    var _this;

    _classCallCheck(this, FinishRegistration);

    _this = _super.apply(this, arguments);
    _this.state = {
      forms: false
    };
    return _this;
  }

  _createClass(FinishRegistration, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.fetchRequest = apiFetch({
        path: "/jensen/v1/ninja/forms"
      }).then(function (result) {
        if (!result) {
          console.log('Error');
          console.log(result);
        }

        _this2.setState({
          forms: JSON.parse(result)
        });
      })["catch"](function () {
        _this2.setState({
          forms: false
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          setAttributes = _this$props.setAttributes,
          attributes = _this$props.attributes,
          backgroundColor = _this$props.backgroundColor,
          setBackgroundColor = _this$props.setBackgroundColor,
          textColor = _this$props.textColor,
          setTextColor = _this$props.setTextColor;
      var nfRegistration = attributes.nfRegistration;
      var inspector = wp.element.createElement(InspectorControls, {
        key: "inspector"
      }, wp.element.createElement(PanelColorSettings, {
        title: __('Background Color'),
        initialOpen: true,
        colorSettings: [{
          value: textColor.color,
          onChange: setTextColor,
          label: __('Text color')
        }, {
          value: backgroundColor.color,
          onChange: setBackgroundColor,
          label: __('Background color')
        }]
      }));
      /*<Disabled>
          <ServerSideRender
              block="jg/register"
              attributes={ attributes }
          />
      </Disabled>*/

      if (this.state.forms) {
        //console.log(this.state.forms)
        return wp.element.createElement(React.Fragment, null, inspector, wp.element.createElement("div", {
          className: "w-50 mx-auto text-center py-4"
        }, wp.element.createElement("h3", null, "User profile"), wp.element.createElement(SelectControl, {
          label: __('Select Registration Form', 'jensen'),
          value: nfRegistration,
          onChange: function onChange(value) {
            return setAttributes({
              nfRegistration: value
            });
          },
          options: this.state.forms
        })));
      }

      return wp.element.createElement(React.Fragment, null, inspector, wp.element.createElement(Spinner, {
        animation: "border",
        role: "status",
        variant: "primary"
      }, wp.element.createElement("span", {
        className: "sr-only"
      }, "Loading...")));
    }
  }]);

  return FinishRegistration;
}(Component);

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(FinishRegistration));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/register/index.jsx":
/*!************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/register/index.jsx ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/register/block.json");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/register/save.jsx");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/register/edit.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies




var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_2__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/register/save.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/register/save.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // Internal dependencies


var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies

function save(props) {
  var attributes = props.attributes,
      className = props.className;
  var backgroundColor = attributes.backgroundColor,
      textColor = attributes.textColor,
      title = attributes.title;
  var style = {
    backgroundColor: backgroundColor && backgroundColor.color,
    textColor: textColor && textColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('wp-block-cover', 'register', className, backgroundColor && "has-".concat(backgroundColor, "-background-color has-background-dim"), textColor && "has-".concat(textColor, "-color has-").concat(textColor, "-text-color"));
  return wp.element.createElement("div", {
    className: classes,
    style: style
  }, wp.element.createElement(InnerBlocks.Content, null));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/resource/edit.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/resource/edit.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Resource; }
/* harmony export */ });
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // WordPress dependencies


var __ = wp.i18n.__;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    InspectorControls = _wp$blockEditor.InspectorControls,
    useBlockProps = _wp$blockEditor.useBlockProps,
    URLPopover = _wp$blockEditor.URLPopover,
    URLInput = _wp$blockEditor.URLInput,
    __experimentalUseInnerBlocksProps = _wp$blockEditor.__experimentalUseInnerBlocksProps;
var _wp$components = wp.components,
    Button = _wp$components.Button,
    PanelBody = _wp$components.PanelBody,
    TextControl = _wp$components.TextControl,
    TextareaControl = _wp$components.TextareaControl;
var _wp$data = wp.data,
    select = _wp$data.select,
    useSelect = _wp$data.useSelect; // Other Dependencies
// Internal dependencies

function Resource(props) {
  var attributes = props.attributes,
      setAttributes = props.setAttributes,
      className = props.className,
      clientId = props.clientId;
  var title = attributes.title,
      blockId = attributes.blockId,
      childClasses = attributes.childClasses,
      text = attributes.text,
      link = attributes.link;
  var blockProps = useBlockProps({
    className: className
  }); //if ( ! blockId ) {

  setAttributes({
    blockId: "resource-".concat(clientId.split('-')[0])
  }); //}

  var _useSelect = useSelect(function (select) {
    return {
      blockCount: select('core/block-editor').getBlockCount(clientId)
    };
  }),
      blockCount = _useSelect.blockCount;

  var innerBlocksProps = __experimentalUseInnerBlocksProps(blockProps, {
    allowedBlocks: ['core/embed', 'core/video']
  });

  return wp.element.createElement(React.Fragment, null, wp.element.createElement(TextControl, {
    label: __('Resource(s) Label'),
    onChange: function onChange(value) {
      return setAttributes({
        title: value
      });
    },
    value: title
  }), wp.element.createElement(TextareaControl, {
    label: __('Resource(s) Description'),
    onChange: function onChange(value) {
      return setAttributes({
        text: value
      });
    },
    value: text
  }), blockCount === 0 && wp.element.createElement(URLInput, {
    label: __('External Link (instead of embed): '),
    value: link,
    onChange: function onChange(nextURL) {
      return setAttributes({
        link: nextURL
      });
    },
    placeholder: '',
    disableSuggestions: true
  }), link === '' || !link && wp.element.createElement("div", innerBlocksProps));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/resource/index.jsx":
/*!************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/resource/index.jsx ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block */ "./roadtonaig-gutenberg/src/blocks/resource/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/resource/edit.jsx");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/resource/save.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies



var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/resource/save.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/resource/save.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // Internal dependencies


var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies

function save(props) {
  var attributes = props.attributes,
      className = props.className;
  var title = attributes.title,
      blockId = attributes.blockId,
      text = attributes.text,
      link = attributes.link;
  var resourceSublabel;

  if (text) {
    resourceSublabel = wp.element.createElement("div", {
      "class": "resource-sublabel"
    }, text);
  }

  var introDiv = wp.element.createElement("div", {
    className: "label collapsed",
    "data-bs-toggle": "collapse",
    href: "#".concat(blockId),
    role: "button",
    "aria-expanded": "false",
    "aria-controls": blockId
  }, wp.element.createElement("span", null, title), wp.element.createElement("button", {
    className: "btn btn-primary"
  }, "Watch Video"), resourceSublabel);
  var childClasses = 'video';

  if (link) {
    introDiv = wp.element.createElement("div", {
      className: "label"
    }, wp.element.createElement("span", null, title), wp.element.createElement("a", {
      className: "btn btn-primary",
      href: "".concat(link),
      target: "_blank"
    }, "Go to Link"), resourceSublabel);
    childClasses = 'link';
  }

  return wp.element.createElement("div", {
    className: classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default()([childClasses, className])
  }, introDiv, wp.element.createElement("div", {
    id: blockId,
    className: "resource-text collapse"
  }, wp.element.createElement(InnerBlocks.Content, null)));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/set-user-data/edit.jsx":
/*!****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/set-user-data/edit.jsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }



function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings;
var _wp = wp,
    apiFetch = _wp.apiFetch;
var _wp$components = wp.components,
    ServerSideRender = _wp$components.ServerSideRender,
    Disabled = _wp$components.Disabled,
    Spinner = _wp$components.Spinner,
    SelectControl = _wp$components.SelectControl;
var compose = wp.compose.compose; // Other Dependencies
// Internal dependencies

var FinishRegistration = /*#__PURE__*/function (_Component) {
  _inherits(FinishRegistration, _Component);

  var _super = _createSuper(FinishRegistration);

  function FinishRegistration() {
    var _this;

    _classCallCheck(this, FinishRegistration);

    _this = _super.apply(this, arguments);
    _this.state = {
      forms: false
    };
    return _this;
  }

  _createClass(FinishRegistration, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.fetchRequest = apiFetch({
        path: "/jensen/v1/ninja/forms"
      }).then(function (result) {
        if (!result) {
          console.log('Error');
          console.log(result);
        }

        _this2.setState({
          forms: JSON.parse(result)
        });
      })["catch"](function () {
        _this2.setState({
          forms: false
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          setAttributes = _this$props.setAttributes,
          attributes = _this$props.attributes,
          backgroundColor = _this$props.backgroundColor,
          setBackgroundColor = _this$props.setBackgroundColor,
          textColor = _this$props.textColor,
          setTextColor = _this$props.setTextColor;
      var nfRegistration = attributes.nfRegistration;
      var inspector = wp.element.createElement(InspectorControls, {
        key: "inspector"
      }, wp.element.createElement(PanelColorSettings, {
        title: __('Background Color'),
        initialOpen: true,
        colorSettings: [{
          value: textColor.color,
          onChange: setTextColor,
          label: __('Text color')
        }, {
          value: backgroundColor.color,
          onChange: setBackgroundColor,
          label: __('Background color')
        }]
      }));

      if (this.state.forms) {
        //console.log(this.state.forms)
        return wp.element.createElement(React.Fragment, null, inspector, wp.element.createElement("div", {
          className: "w-50 mx-auto text-center py-4"
        }, wp.element.createElement("h3", null, "User profile"), wp.element.createElement(SelectControl, {
          label: __('Select Registration Form', 'jensen'),
          value: nfRegistration,
          onChange: function onChange(value) {
            return setAttributes({
              nfRegistration: value
            });
          },
          options: this.state.forms
        })), wp.element.createElement(Disabled, null, wp.element.createElement(ServerSideRender, {
          block: "jg/set-user-data",
          attributes: attributes
        })));
      }

      return wp.element.createElement(React.Fragment, null, inspector, wp.element.createElement(Spinner, {
        animation: "border",
        role: "status",
        variant: "primary"
      }, wp.element.createElement("span", {
        className: "sr-only"
      }, "Loading...")));
      return wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
        key: "inspector"
      }, wp.element.createElement(PanelColorSettings, {
        title: __('Background Color'),
        initialOpen: true,
        colorSettings: [{
          value: textColor.color,
          onChange: setTextColor,
          label: __('Text color')
        }, {
          value: backgroundColor.color,
          onChange: setBackgroundColor,
          label: __('Background color')
        }]
      }), wp.element.createElement(PanelBody, {
        title: __('Ninja Form'),
        initialOpen: false
      }, wp.element.createElement(TextControl, {
        label: __('Registration'),
        value: nfRegistration,
        onChange: function onChange(value) {
          return setAttributes({
            nfRegistration: value
          });
        }
      }))));
    }
  }]);

  return FinishRegistration;
}(Component);

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(FinishRegistration));

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/set-user-data/index.jsx":
/*!*****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/set-user-data/index.jsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/set-user-data/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/set-user-data/edit.jsx");
// External dependencies
// WordPress dependencies
var __ = wp.i18n.__; // Internal dependencies




var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/sign-in/edit.jsx":
/*!**********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/sign-in/edit.jsx ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Signin; }
/* harmony export */ });

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    MediaUpload = _wp$blockEditor.MediaUpload,
    MediaUploadCheck = _wp$blockEditor.MediaUploadCheck,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings;
var _wp$components = wp.components,
    ServerSideRender = _wp$components.ServerSideRender,
    PanelBody = _wp$components.PanelBody,
    TextControl = _wp$components.TextControl,
    Disabled = _wp$components.Disabled,
    Button = _wp$components.Button;
var compose = wp.compose.compose;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select; // Other Dependencies
// Internal dependencies

function Signin(_ref) {
  var attributes = _ref.attributes,
      setAttributes = _ref.setAttributes;
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(Disabled, null, wp.element.createElement(ServerSideRender, {
    block: "jg/signin",
    attributes: attributes
  })));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/sign-in/index.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/sign-in/index.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./roadtonaig-gutenberg/src/blocks/sign-in/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/sign-in/edit.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies



var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training-subblock/edit.jsx":
/*!********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training-subblock/edit.jsx ***!
  \********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    useBlockProps = _wp$blockEditor.useBlockProps,
    useInnerBlocksProps = _wp$blockEditor.__experimentalUseInnerBlocksProps;
var TextControl = wp.components.TextControl; // Other Dependencies
// Internal dependencies

var ALLOWED_BLOCKS = ['core/column', 'core/paragraph', 'core/heading', 'core/list', 'core/group', 'core/separator', 'core/spacer', 'core/buttons', 'core/image', 'core/video'];

function TrainingSubblock(_ref) {
  var title = _ref.attributes.title,
      setAttributes = _ref.setAttributes,
      className = _ref.className;
  var blockProps = useBlockProps({
    className: className
  });
  var innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
    template: [['core/paragraph', {
      placeholder: __('Add Content...')
    }]]
  });
  return wp.element.createElement(React.Fragment, null, wp.element.createElement("div", blockProps, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement("div", null, wp.element.createElement(TextControl, {
    label: __('Section Title'),
    onChange: function onChange(value) {
      return setAttributes({
        title: value
      });
    },
    value: title
  })), wp.element.createElement("div", innerBlocksProps))));
}

/* harmony default export */ __webpack_exports__["default"] = (TrainingSubblock);

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training-subblock/index.jsx":
/*!*********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training-subblock/index.jsx ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block */ "./roadtonaig-gutenberg/src/blocks/training-subblock/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/training-subblock/edit.jsx");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/training-subblock/save.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies



var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training-subblock/save.jsx":
/*!********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training-subblock/save.jsx ***!
  \********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // Internal dependencies


var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies

function save(_ref) {
  var className = _ref.className,
      title = _ref.attributes.title;
  return wp.element.createElement("div", {
    className: className
  }, wp.element.createElement("div", {
    className: "label"
  }, title), wp.element.createElement("div", {
    className: "content"
  }, wp.element.createElement(InnerBlocks.Content, null)));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training/edit.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training/edit.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Training; }
/* harmony export */ });
/* harmony import */ var _components_date__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/date */ "./roadtonaig-gutenberg/src/blocks/components/date.jsx");
/* harmony import */ var _components_buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/buttons */ "./roadtonaig-gutenberg/src/blocks/components/buttons.jsx");
// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var compose = wp.compose.compose;
var useEffect = wp.element.useEffect;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    useBlockProps = _wp$blockEditor.useBlockProps,
    InspectorControls = _wp$blockEditor.InspectorControls,
    useInnerBlocksProps = _wp$blockEditor.__experimentalUseInnerBlocksProps;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    TextControl = _wp$components.TextControl,
    TextareaControl = _wp$components.TextareaControl,
    ToggleControl = _wp$components.ToggleControl,
    RadioControl = _wp$components.RadioControl; // Other Dependencies
// Internal dependencies




var ALLOWED_BLOCKS = ['core/column', 'core/paragraph', 'core/heading', 'core/list', 'core/group', 'core/separator', 'core/spacer', 'core/buttons', 'core/image', 'core/video', 'jg/resource', 'jg/training-subblock'];
function Training(_ref) {
  var attributes = _ref.attributes,
      _ref$attributes = _ref.attributes,
      blockId = _ref$attributes.blockId,
      title = _ref$attributes.title,
      type = _ref$attributes.type,
      setAttributes = _ref.setAttributes,
      clientId = _ref.clientId,
      className = _ref.className;
  useEffect(function () {
    //This conditional is useful to only set the id attribute once when the component mounts for the first time
    var tempBlockId = "block-".concat(clientId.split('-')[0]);
    blockId !== tempBlockId && setAttributes({
      blockId: tempBlockId
    });
  }, []);
  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelBody, {
    title: __('User/Time Restrictions')
  }, (0,_components_date__WEBPACK_IMPORTED_MODULE_0__.datePanel)(attributes, setAttributes)), wp.element.createElement(PanelBody, {
    title: __('Complete/upload buttons')
  }, (0,_components_buttons__WEBPACK_IMPORTED_MODULE_1__.buttonsControl)(attributes, setAttributes)), wp.element.createElement(PanelBody, {
    title: __('Content')
  }, wp.element.createElement(RadioControl, {
    label: "Type",
    selected: type,
    options: [{
      label: 'Activity',
      value: 'completed_activities'
    }, {
      label: 'Training',
      value: 'completed_training'
    }],
    onChange: function onChange(value) {
      return setAttributes({
        type: value
      });
    }
  }))));
  var blockProps = useBlockProps({
    className: className
  });
  var innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
    template: [['core/paragraph', {
      placeholder: __('Add Content...')
    }]]
  });
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", blockProps, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement(TextControl, {
    label: __('Accordion Title'),
    onChange: function onChange(value) {
      return setAttributes({
        title: value
      });
    },
    value: title
  }), wp.element.createElement("div", innerBlocksProps))));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training/index.jsx":
/*!************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training/index.jsx ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block */ "./roadtonaig-gutenberg/src/blocks/training/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./roadtonaig-gutenberg/src/blocks/training/edit.jsx");
/* harmony import */ var _save__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./save */ "./roadtonaig-gutenberg/src/blocks/training/save.jsx");
// External dependencies
// WordPress dependencies
// Internal dependencies



var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  save: _save__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training/save.jsx":
/*!***********************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training/save.jsx ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ save; }
/* harmony export */ });
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
// External dependencies
 // Internal dependencies


var __ = wp.i18n.__;
var InnerBlocks = wp.blockEditor.InnerBlocks; // Internal dependencies

function save(_ref) {
  var className = _ref.className,
      title = _ref.attributes.title;
  return wp.element.createElement("div", {
    className: className
  }, wp.element.createElement("div", {
    className: "content"
  }, wp.element.createElement(InnerBlocks.Content, null)));
}

/***/ }),

/***/ "./roadtonaig-gutenberg/src/styles/app.scss":
/*!**************************************************!*\
  !*** ./roadtonaig-gutenberg/src/styles/app.scss ***!
  \**************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/editor.scss":
/*!*****************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/editor.scss ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/content-visibility/block.json":
/*!***********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/content-visibility/block.json ***!
  \***********************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/content-visibility","category":"common","title":"Variable Visibility","description":"Show only to certain users","keywords":["jensen","wa","roadtonaig"],"icon":"welcome-view-site","attributes":{"userOnly":{"type":"boolean","default":false},"timeRestrictionLabel":{"type":"string","default":""},"adminOnly":{"type":"boolean","default":false},"restrictGlobal":{"type":"boolean","default":false},"restrictCustomStart":{"type":"boolean","default":false},"restrictCustomEnd":{"type":"boolean","default":false},"dateStart":{"type":"string","default":false},"dateEnd":{"type":"string","default":false},"backgroundColor":{"type":"string","default":"white"},"textColor":{"type":"string","default":"black"},"align":{"type":"string","default":"full"}},"supports":{"customClassName":true,"align":["center","wide"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/member-complete/block.json":
/*!********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/member-complete/block.json ***!
  \********************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/member-complete","title":"Member Complete","description":"Allow activity completion by member","icon":"saved","category":"common","keywords":["jensen","wa","roadtonaig"],"attributes":{"align":{"type":"string","default":"left"},"backgroundColor":{"type":"string","default":false},"textColor":{"type":"string","default":false},"completeType":{"type":"string","default":"initial-fitness"},"type":{"type":"string","default":"completed_training"}},"supports":{"customClassName":true,"align":false}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/page-heading/block.json":
/*!*****************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/page-heading/block.json ***!
  \*****************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/page-heading","title":"Page Intro","category":"widgets","icon":"common","description":"Intro block with page title","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{"align":{"type":"string","default":"full"},"title":{"type":"string","default":""},"backgroundColor":{"type":"string","default":"secondary"}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/post-carousel/block.json":
/*!******************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/post-carousel/block.json ***!
  \******************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/post-carousel","title":"Post Carousel","category":"common","icon":"tagcloud","description":"Show a carousel of posts","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{"postType":{"type":"string","default":"challenge"},"numSlides":{"type":"array","default":6},"backgroundColor":{"type":"string","default":false},"textColor":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/post-grid/block.json":
/*!**************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/post-grid/block.json ***!
  \**************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/post-grid","title":"Post Grid","category":"common","icon":"tagcloud","description":"Show a grid of posts","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{"postType":{"type":"string","default":"challenge"},"backgroundColor":{"type":"string","default":false},"textColor":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/profile/block.json":
/*!************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/profile/block.json ***!
  \************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/profile","title":"Profile","category":"common","description":"Allow users to edit their profile","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{"align":{"type":"string","default":"left"},"backgroundColor":{"type":"string","default":false},"textColor":{"type":"string","default":false},"category":{"type":"string","default":false},"nfProfile":{"type":"string","default":false},"nfPassword":{"type":"string","default":false},"nfComment":{"type":"string","default":false},"nfPost":{"type":"string","default":false},"nfFamily":{"type":"string","default":false}},"supports":{"customClassName":true,"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/register/block.json":
/*!*************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/register/block.json ***!
  \*************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/register","title":"Registration form","category":"widgets","description":"Sign up for activities","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{"align":{"type":"string","default":"left"},"backgroundColor":{"type":"string","default":false},"textColor":{"type":"string","default":false},"nfRegistration":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/resource/block.json":
/*!*************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/resource/block.json ***!
  \*************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/resource","category":"common","icon":"admin-site-alt3","title":"Resource","description":"Resource","keywords":["jensen","wa","roadtonaig"],"attributes":{"blockId":{"type":"string"},"title":{"type":"string"},"link":{"type":"string"}},"supports":{"customClassName":true,"align":false}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/set-user-data/block.json":
/*!******************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/set-user-data/block.json ***!
  \******************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/set-user-data","title":"Finish Registration","category":"common","icon":"tagcloud","description":"Finish user registration after email confirmation","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/sign-in/block.json":
/*!************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/sign-in/block.json ***!
  \************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"name":"jg/signin","title":"Sign in form","category":"common","description":"Show form","keywords":["jensen","wa","roadtonaig"],"textdomain":"default","attributes":{},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training-subblock/block.json":
/*!**********************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training-subblock/block.json ***!
  \**********************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/training-subblock","category":"common","parent":"jg/training","icon":"align-wide","title":"Training Subblock","description":"Heading and content inside main block","keywords":["jensen","wa","roadtonaig"],"attributes":{"title":{"type":"string","default":""}},"supports":{"customClassName":true,"align":false}}');

/***/ }),

/***/ "./roadtonaig-gutenberg/src/blocks/training/block.json":
/*!*************************************************************!*\
  !*** ./roadtonaig-gutenberg/src/blocks/training/block.json ***!
  \*************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/training","category":"common","keywords":["jensen","wa","roadtonaig"],"title":"Training","description":"One day\'s training","icon":"universal-access","attributes":{"blockId":{"type":"string"},"title":{"type":"string","default":""},"userOnly":{"type":"boolean","default":false},"timeRestrictionLabel":{"type":"string","default":""},"adminOnly":{"type":"boolean","default":false},"restrictGlobal":{"type":"boolean","default":false},"restrictCustomStart":{"type":"boolean","default":false},"restrictCustomEnd":{"type":"boolean","default":false},"dateStart":{"type":"string","default":""},"dateEnd":{"type":"string","default":""},"uploadButton":{"type":"boolean","default":false},"uploadType":{"type":"string","default":"initial-fitness"},"nfUpload":{"type":"string","default":""},"completeButton":{"type":"boolean","default":false},"completeType":{"type":"string","default":"initial-fitness"},"type":{"type":"string","default":"completed_training"}},"supports":{"customClassName":true,"align":false}}');

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["assets/styles/editor","assets/styles/app","/assets/scripts/vendor"], function() { return __webpack_exec__("./roadtonaig-gutenberg/src/blocks/blocks.jsx"), __webpack_exec__("./roadtonaig-gutenberg/src/styles/app.scss"), __webpack_exec__("./roadtonaig-gutenberg/src/blocks/editor.scss"); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);