<?php
/**
 * @wordpress-plugin
 * Plugin Name: Road to NAIG Gutenberg blocks
 * Plugin URI: https://jensengroup.ca/
 * Description: Gutenberg blocks for theme use
 * Version:     2021.7
 * Author:      Jensen Group
 * License:     GPL-2.0+
 * License URI: http:// www.gnu.org/licenses/gpl-2.0.txt
 * @package   jg-Gutenberg
 *
 * @author    Jensen Group
 * @copyright 2021 Jensen Group
 * @license   GPL-2.0+
 */
namespace jg\Plugin\Gutenberg;

defined( __NAMESPACE__ . '\URI' ) || define( __NAMESPACE__ . '\URI', plugin_dir_url( __FILE__ ) );
defined( __NAMESPACE__ . '\PATH' ) || define( __NAMESPACE__ . '\PATH', plugin_dir_path( __FILE__ ) );

$files = ['blocks/helpers', 'assets',
    'cpt/workshop',
    'blocks/profile/app', 'blocks/profile/dashboard', 'blocks/profile/family',
    'blocks/misc/training', 'blocks/misc/visibility',
    'blocks/member/upload', 'blocks/member/complete',
    'blocks/registration/complete', 'blocks/registration/register', 'blocks/registration/sign-in',
    'blocks/post/carousel', 'blocks/post/grid',
    'rest',
];
foreach ( $files as $file ) {
    $file = PATH . 'app/' . $file . '.php';

    if ( file_exists( $file ) ) {
        require_once $file;
    }
}